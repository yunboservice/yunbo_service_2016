//
//  RlAccountModel.h
//  CarService
//
//  Created by apple on 15/7/10.
//  Copyright (c) 2015年 fenglun. All rights reserved.
//  容连账户信息

#import "YHBaseModel.h"

@interface RlAccountModel : YHBaseModel<NSCoding>

@property (nonatomic ,copy) NSString *subAccount;
@property (nonatomic ,copy) NSString *subToken;
@property (nonatomic ,copy) NSString *voipAccount;
@property (nonatomic ,copy) NSString *voipPwd;
/**
 获取容联帐号信息
 */
+(RlAccountModel*)getRlAcount;
/**
 保存容联帐号信息
 */
+(void)saveRlAcoun:(NSDictionary*)dataDic;


@end
