//
//  RlAccountModel.m
//  CarService
//
//  Created by apple on 15/7/10.
//  Copyright (c) 2015年 fenglun. All rights reserved.
//

#import "RlAccountModel.h"
#define RlcountPath  [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Rlcount.data"]
@implementation RlAccountModel


+(RlAccountModel*)getRlAcount{

    return (RlAccountModel*)[NSKeyedUnarchiver unarchiveObjectWithFile:RlcountPath];
    
}

+(void)saveRlAcoun:(NSDictionary*)dataDic{
    RlAccountModel *model = [[RlAccountModel alloc]init];
    [model setValuesForKeysWithDictionary:dataDic];
    [NSKeyedArchiver archiveRootObject:model toFile:RlcountPath];
   
}


-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:_subAccount forKey:@"subAccount"];
    [aCoder encodeObject:_subToken forKey:@"subToken"];
    [aCoder encodeObject:_voipAccount forKey:@"voipAccount"];
    [aCoder encodeObject:_voipPwd forKey:@"voipPwd"];
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]) {
        _subAccount = [aDecoder decodeObjectForKey:@"subAccount"];
        _subToken = [aDecoder decodeObjectForKey:@"subToken"];
        _voipAccount = [aDecoder decodeObjectForKey:@"voipAccount"];
        _voipPwd = [aDecoder decodeObjectForKey:@"voipPwd"];
    }
    return self ;
}


@end
