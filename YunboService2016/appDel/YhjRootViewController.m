//
//  YhjRootViewController.m
//  yunbo2016
//
//  Created by apple on 15/10/10.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import "YhjRootViewController.h"
#import "MainViewController.h"
#import "PlunderViewController.h"
#import "OrderListViewController.h"
#import "RDVTabBarItem.h"
//#import "EAIntroView.h"
#import <UIImage+GIF.h>
#import "YHNavgationController.h"
//#import "SessionViewController.h"
#import "YHJHelp.h"
#import "CommenHead.h"
#import "WWYLogInViewController.h"


@interface YhjRootViewController ()<RDVTabBarControllerDelegate>
{
    __block NSInteger _currentIndex;
}

@property (nonatomic, assign) NSInteger currentIndex;


@end

@implementation YhjRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //    设置委托
    id<RDVTabBarControllerDelegate> __unsafe_unretained delegate = self;
    self.delegate = delegate;
    

     [self setupViewControllers];
    
    

    //    if ([YHJHelp isFirstLoad]) {
    //        [self showIntroWithCrossDissolve];
    //    }
}

-(void)setupViewControllers
{
    MainViewController *yun = [[MainViewController alloc]init];
    yun.isRootVC = YES;
    YHNavgationController *yunNav  = [[YHNavgationController alloc]initWithRootViewController:yun];
    
    PlunderViewController *plunder = [[PlunderViewController alloc]init];
    plunder.isRootVC = YES;
    YHNavgationController *plunderNav  = [[YHNavgationController alloc]initWithRootViewController:plunder];
    
    
    OrderListViewController *orderList = [[OrderListViewController alloc]init];
    orderList.isRootVC = YES;
    YHNavgationController *orderListNav  = [[YHNavgationController alloc]initWithRootViewController:orderList];
    
    [self setViewControllers:@[yunNav,plunderNav, orderListNav]];
    [self configTableBarController];
    
}


-(void)configTableBarController
{
    int index = 0;
    NSMutableDictionary *textAttrs = [NSMutableDictionary dictionary];
    textAttrs[NSForegroundColorAttributeName] =KColorRGB(123, 123, 123);
    NSMutableDictionary *selectTextAttrs = [NSMutableDictionary dictionary];
    selectTextAttrs[NSForegroundColorAttributeName] = KColorRGB(92, 181, 49);
    NSArray *titleArr = @[@"主页",@"抢单",@"订单"];
    for (RDVTabBarItem *item in [[self tabBar] items]) {
        item.unselectedTitleAttributes = textAttrs;
        item.selectedTitleAttributes = selectTextAttrs;
        item.title = titleArr[index];
        UIImage *selectedimage = [UIImage imageNamed:[NSString stringWithFormat:@"home_%d.png",index+1]];
        UIImage *unselectedimage = [UIImage imageNamed:[NSString stringWithFormat:@"home_%d_un.png",index+1]];
        [item setFinishedSelectedImage:selectedimage withFinishedUnselectedImage:unselectedimage];
        index++;
    }
    
}



-(NSData*)dataWithGigName:(NSString*)imageName{
    
    NSString  *filePath = [[NSBundle bundleWithPath:[[NSBundle mainBundle] bundlePath]] pathForResource:imageName ofType:nil];
    NSData  *imageData = [NSData dataWithContentsOfFile:filePath];
    return imageData;
}


#pragma mark --RDVTabBarDelegate
//- (BOOL)tabBar:(RDVTabBar *)tabBar shouldSelectItemAtIndex:(NSInteger)index {
//    if(index==0){
//        [MobClick event:@"YunBoMain_Home"];
//    }else if(index==1){
//        [MobClick event:@"YunBoMain_Friend"];
//    }else if(index==2){
//        [MobClick event:@"YunBoMain_Find"];
//    }else if(index==3){
//        [MobClick event:@"YunBoMain_Mine"];
//    }
//    
//    _currentIndex = index;
//    if (_currentIndex>0) {
//        return [YHJHelp isLoginAndIsNetValiadWitchController:self];
//    }else{
//        
//        return YES;
//    }
//}







@end
