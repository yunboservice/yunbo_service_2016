//
//  AppDelegate.m
//  YunboService2016
//
//  Created by apple on 3/2/16.
//  Copyright © 2016 CarService. All rights reserved.
//

#import "AppDelegate.h"
#import "AppDelegate+threePort.h"
#import "YhjRootViewController.h"
#import "WWYLogInViewController.h"
#import "YHNavgationController.h"
 

@interface AppDelegate ()
@property (nonatomic, strong) YhjRootViewController *rootVc;


@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];

    NSLog(@"userID %@", KgetUserValueByParaName(USERID));
    
    // 确定登录状态
    if (KgetUserValueByParaName(USERID)){
        NSLog(@"已登录");
        _rootVc = [[YhjRootViewController alloc]init];
        self.window.rootViewController  = _rootVc;
        // 登录成功开启百度地图
        [self BMKMapConfig];
        // 开启友盟用户统计
        [self UMengConfig];
        // 容联回调
        
        
    }else{
        NSLog(@"执行登录流程");
        WWYLogInViewController *loginVc = [[WWYLogInViewController alloc] init];
        // 修改根式图
        [loginVc actionWhenLoginSucceed:^{
            [self changeRootVC];
        }];
        YHNavgationController *loginNav = [[YHNavgationController alloc] initWithRootViewController:loginVc];
        self.window.rootViewController = loginNav;
    }
    
    
    
    [self.window makeKeyAndVisible];
    return YES;
}


- (void)changeRootVC{
    _rootVc = [[YhjRootViewController alloc]init];
    self.window.rootViewController = _rootVc;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [BMKMapView willBackGround];//当应用即将后台时调用，停止一切调用opengl相关的操作

}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     [BMKMapView didForeGround];//当应用恢复前台状态时调用，回复地图的渲染和opengl相关的操作
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
