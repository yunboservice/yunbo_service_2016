//
//  AppDelegate+threePort.h
//  YunboService2016
//
//  Created by 王维一 on 16/3/30.
//  Copyright © 2016年 CarService. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (threePort)

- (void)BMKMapConfig;
- (void)UMengConfig;
- (void)ECDeviceConfig;

@end
