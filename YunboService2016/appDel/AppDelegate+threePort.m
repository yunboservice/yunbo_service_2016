//
//  AppDelegate+threePort.m
//  YunboService2016
//
//  Created by 王维一 on 16/3/30.
//  Copyright © 2016年 CarService. All rights reserved.
//

#import "AppDelegate+threePort.h"
#import "DeviceDelegateHelper.h"


@interface AppDelegate ()<BMKGeneralDelegate, ECDeviceDelegate>




@end

@implementation AppDelegate (threePort)

- (void)BMKMapConfig{
    
    // 如果要关注网络及授权验证事件，请设定     generalDelegate参数
    BOOL ret = [[[BMKMapManager alloc] init] start:@"HLDYBCAzbTgHLqKimsdxpd15"  generalDelegate:nil];
    if (!ret) {
        NSLog(@"manager start failed!");
    }else{
        NSLog(@"地图开启成功");
    }
}

- (void)UMengConfig{
    [MobClick startWithAppkey:@"56e7b65167e58e09eb001b6e" reportPolicy:BATCH   channelId:nil];
}

- (void)ECDeviceConfig{
    [ECDevice sharedInstance].delegate = [DeviceDelegateHelper sharedInstance];
    
}


@end
