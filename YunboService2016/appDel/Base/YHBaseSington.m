//
//  YHBaseSington.m
//  yunbo2016
//
//  Created by apple on 15/11/23.
//  Copyright © 2015年 apple. All rights reserved.
//

#import "YHBaseSington.h"
#import <MMProgressHUD/MMProgressHUD.h>
#import <MMProgressHUD/MMProgressHUDOverlayView.h>
#import <MMProgressHUD/MMRadialProgressView.h>
#import <MMProgressHUD/MMLinearProgressView.h>


@implementation YHBaseSington

+(instancetype)shareInstance
{
    return [[super alloc]init];
}

- (void)showHUDWithMessage:(NSString*)message {
    //    [MMProgressHUD setDisplayStyle:MMProgressHUDDisplayStylePlain];
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleFade];
    [MMProgressHUD showWithTitle:message status:@""];
    
    
}
//@"Success!"
- (void)hideHUD{
    
    [MMProgressHUD dismiss];
}


//@"Success!"
- (void)hideHUDWithMessage:(NSString*)message {
    
    [MMProgressHUD dismissWithSuccess:@"  "];
}


- (void)showAlterWithMessage:(NSString*)message{
    UIAlertView *alter = [[UIAlertView alloc] initWithTitle:@"提示" message:message delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
    [alter show];
}


@end
