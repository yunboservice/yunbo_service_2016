//
//  YHBaseModel.h
//  yunbo2016
//
//  Created by apple on 15/11/18.
//  Copyright © 2015年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJExtension.h"

@interface YHBaseModel : NSObject

-(void)setValue:(id)value forUndefinedKey:(NSString *)key;


@end
