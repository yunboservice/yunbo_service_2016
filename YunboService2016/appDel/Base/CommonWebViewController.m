//
//  CommonWebViewController.m
//  yunbo2016
//
//  Created by apple on 15/12/1.
//  Copyright © 2015年 apple. All rights reserved.
//  共用webView

#import "CommonWebViewController.h"
#import "WebViewJavascriptBridge.h"

@interface CommonWebViewController ()<UIWebViewDelegate>

@property WebViewJavascriptBridge* bridge;

@property (nonatomic ,copy) NSString *urlStr;
@property (nonatomic ,copy) NSString *NavTitle;

@end
@implementation CommonWebViewController


-(instancetype)initWithUrlStr:(NSString *)urlStr andNavTitle:(NSString *)navTitle

{
    if (self= [super init]) {
        _urlStr = urlStr;
        _NavTitle = navTitle;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.sc_navigationItem.title = NSLocalizedString(_NavTitle, nil);
    
    _web = [[UIWebView alloc]init];
    _web.scrollView.bounces = NO;
    _web.delegate   = self;
    _web.scrollView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:_web];
    [_web  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(UIEdgeInsetsMake(KTopHeight, 0, 0, 0));
    }];
    NSURLRequest *requset = [NSURLRequest requestWithURL:[NSURL URLWithString:_urlStr]];
    [_web loadRequest:requset];
    
    [WebViewJavascriptBridge enableLogging];
    
    _bridge = [WebViewJavascriptBridge bridgeForWebView:self.web handler:^(id data, WVJBResponseCallback responseCallback) {
        
    }];
    
    @weakify(self);
    [_bridge registerHandler:@"share" handler:^(id data, WVJBResponseCallback responseCallback) {
        responseCallback(@"ok");
        @strongify(self);
        [self shareWithWebDataAction:data];
    }];
    
}


-(void)shareWithWebDataAction:(NSDictionary*)dataDic{
    if (dataDic) {
//        [YHJHelp showShareInController:self andShareURL:[NSString stringWithFormat:@"%@",[dataDic objectForKey:@"url"]] andTitle:[dataDic objectForKey:@"title"] andShareText:[dataDic objectForKey:@"content"] andShareImage:[self getImageFromURL:[dataDic objectForKey:@"imageurl"]]];
        
    }
}

-(UIImage *) getImageFromURL:(NSString *)fileURL {
    NSLog(@"执行图片下载函数");
    UIImage * result;
    
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileURL]];
    result = [UIImage imageWithData:data];
    
    return result;
}



- (void)webViewDidStartLoad:(UIWebView *)webView{
    [MBProgressHUD showHUDAddedTo:_web animated:YES];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [MBProgressHUD hideHUDForView:_web animated:YES];
}
@end
