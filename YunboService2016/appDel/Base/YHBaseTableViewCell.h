//
//  YHBaseTableViewCell.h
//  yunbo2016
//
//  Created by apple on 15/11/18.
//  Copyright © 2015年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol YHBaseTableViewCellConfig <NSObject>

@required

/**
 *  根据传入的数据模型 cell
 *
 *  @param cellModel 传入的数据模型
 */
- (void)configWithModel:(id)cellModel;


@end

@interface YHBaseTableViewCell : UITableViewCell<YHBaseTableViewCellConfig>

+ (CGFloat)cellHeight;


@end
