//
//  YHBaseSington.h
//  yunbo2016
//
//  Created by apple on 15/11/23.
//  Copyright © 2015年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^FinishBlock)(id send);
@interface YHBaseSington : NSObject

// .h文件
#define XPSingletonH(name) +(instancetype)shared##name;

// .m文件
#define XPSingletonM(name) \
static id _instance; \
\
+ (id)allocWithZone:(struct _NSZone *)zone \
{ \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
_instance = [super allocWithZone:zone]; \
}); \
return _instance; \
} \
\
+ (instancetype)shared##name \
{ \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
_instance = [[self alloc] init]; \
}); \
return _instance; \
} \
\
- (id)copyWithZone:(NSZone *)zone \
{ \
return _instance; \
}

+(instancetype)shareInstance;

- (void)showHUDWithMessage:(NSString*)message;
- (void)hideHUDWithMessage:(NSString*)message;
- (void)hideHUD;
- (void)showAlterWithMessage:(NSString*)message;

@end
