//
//  YHBaseViewController.m
//  yunbo2016
//
//  Created by apple on 15/10/12.
//  Copyright © 2015年 apple. All rights reserved.
//

#import "YHBaseViewController.h"
#import <MMProgressHUD/MMProgressHUD.h>
#import <MMProgressHUD/MMProgressHUDOverlayView.h>
#import <MMProgressHUD/MMRadialProgressView.h>
#import <MMProgressHUD/MMLinearProgressView.h>
#import "GpsManager.h"
@interface YHBaseViewController ()<UIGestureRecognizerDelegate>

@end

@implementation YHBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = KColorRGB(239, 239, 244);
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    [self configNaviBar];
    
}


- (void)configNaviBar{
    if ((self.isRootVC || !self.sc_navigationBar.superview) && self.sc_navigationBar) {
        [self.view addSubview:self.sc_navigationBar];
        [self.sc_navigationBar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@0);
            make.right.equalTo(@0);
            make.top.equalTo(@0);
            make.height.equalTo(KTopHeight);
        }];
    }
}



#pragma mark - Private Helper
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (!self.isRootVC) {
        [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
        
    }else
    {
        [[self rdv_tabBarController] setTabBarHidden:NO animated:NO];

    }
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)refreshData{
    
}

- (void)actSuccess{
    UIViewController *popedVC = [self.navigationController popViewControllerAnimated:YES];
    if (!popedVC) {
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }else {
        YHBaseViewController *lastVC = (YHBaseViewController *)[self.navigationController visibleViewController];
        if ([lastVC isKindOfClass:[YHBaseViewController class]]) {
            [lastVC refreshData];
        }
    }
}

-(void)popView:(NSString*)msg
{
    if(msg==nil || msg.length==0)
        return;
    
    UILabel* view=nil;
    UIView* bgView=nil;
    CGSize viewsize;
    CGSize max=CGSizeMake(150, 80);
    //    viewsize = [msg sizeWithFont:[UIFont fontWithName:@"Arial-BoldMT" size:15]constrainedToSize:max lineBreakMode:NSLineBreakByWordWrapping];
    viewsize = [msg boundingRectWithSize:max options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Arial-BoldMT" size:15]} context:nil].size;
    
    NSInteger viewW = 80;
    NSInteger viewH = 80;
    viewW=viewW>viewsize.width?viewW:viewsize.width;
    viewH=viewH>viewsize.height?viewH:viewsize.height;
    
    if(viewW>60)//留出左右间隔
        viewW+=30;
    
    bgView = [[UILabel alloc] init];
    [bgView layer].cornerRadius = 3;
    [bgView layer].masksToBounds = YES;
    bgView.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    view = [[UILabel alloc] initWithFrame:CGRectMake(15, 0,viewW-30,viewH)];
    [view setBackgroundColor:[UIColor clearColor]];
    
    
    [view setText:msg];
    view.numberOfLines=2;
    view.textColor = [UIColor whiteColor];
    [view setFont:[UIFont fontWithName:@"Arial-BoldMT" size:15]];
    view.textAlignment = NSTextAlignmentCenter;
    
    [bgView addSubview:view];
    [self.view.window addSubview:bgView];
    
    [bgView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(0);
        make.size.equalTo(CGSizeMake(viewW, viewH));
    }];
    bgView.alpha=0;
    bgView.transform = CGAffineTransformMakeScale(1.9,1.9);
    //进入动画
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3];
    bgView.transform = CGAffineTransformMakeScale(1,1);
    bgView.alpha=1;
    [UIView commitAnimations];
    
    //消失动画
    [self performSelector:@selector(popViewDisAnima:)withObject:bgView afterDelay:1.7];
    
    
}

-(void)popViewDisAnima:(UIView*)view
{
    //消失动画
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3];
    view.transform = CGAffineTransformMakeScale(0.1,0.1);
    view.alpha=0;
    [UIView commitAnimations];
    [self performSelector:@selector(closeToast:)withObject:view afterDelay:0.4];
}

-(void)closeToast:(UIView*)view;
{
    [view removeFromSuperview];
}

#pragma mark handle error message
- (void)showErrorMessage:(NSString *)message {
    
    [self showErrorMessage:message withDelegate:nil];
}



- (void)showErrorMessage:(NSString *)message withDelegate:(id)delegate{
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"云泊", @"title") message:message delegate:delegate cancelButtonTitle:NSLocalizedString(@"好", @"title") otherButtonTitles:nil];
    [alertView show];
    
    
}
#pragma mark --HUD
- (void)showHUDWithMessage:(NSString*)message {
    
    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleNone];
    [MMProgressHUD showWithTitle:message status:nil];
    
}
//@"Success!"
- (void)hideHUDWithMessage:(NSString*)message {
    
    [MMProgressHUD dismissWithSuccess:message];
}


-(void)updateUserLoction
{
    
    
    [[GpsManager sharedGpsManager]getLocationOnceBolck:^(BMKUserLocation *location) {
        if (location) {
            double log = location.location.coordinate.longitude;
            double lat = location.location.coordinate.latitude;
            
            KsetUserValueByParaName([NSNumber numberWithDouble:log], HCuserLoctionLng);
            KsetUserValueByParaName([NSNumber numberWithDouble:lat], HCuserLoctionLat);
        }
        
    }];
}


-(void)dealloc
{
    NSLog(@"%@\n🌚退出🌚", NSStringFromClass([self class]));
}

-(void)showMessage:(NSString*)message
{
    if (IsEmptyStr(message)) {
        return;
    }
    
    if (message.length >=10) {
        alertContent(message);
    }else
    {
        [self popView:message];
    }
}



@end
