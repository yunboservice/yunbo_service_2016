//
//  YHBaseTableViewCell.m
//  yunbo2016
//
//  Created by apple on 15/11/18.
//  Copyright © 2015年 apple. All rights reserved.
//

#import "YHBaseTableViewCell.h"

@implementation YHBaseTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)configWithModel:(id)cellModel{
    
}

+ (CGFloat)cellHeight
{
    return 44;
}


@end
