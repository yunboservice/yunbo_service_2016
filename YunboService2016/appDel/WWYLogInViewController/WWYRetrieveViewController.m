//
//  WWYRetrieveViewController.m
//  YunboService2016
//
//  Created by Daydream on 16/3/4.
//  Copyright © 2016年 CarService. All rights reserved.
//

#import "WWYRetrieveViewController.h"

#define WLABELWIDTH KScreenWidth / 4
#define CountTimeForMassage 60

@interface WWYRetrieveViewController (){
    //验证码
    NSInteger testingCode;
}

@property (nonatomic, strong) UIView *numBack;
@property (nonatomic, strong) UILabel *phoneNum;
@property (nonatomic, strong) UITextField *numTF;
@property (nonatomic, strong) UIView *passWordBack;
@property (nonatomic, strong) UILabel *passWord;
@property (nonatomic, strong) UITextField *passWordTF;
@property (nonatomic, strong) UIView *checkBack;
@property (nonatomic, strong) UILabel *checkPW;
@property (nonatomic, strong) UITextField *checkTF;
@property (nonatomic, strong) UIView *testBack;
@property (nonatomic, strong) UILabel *testCode;
@property (nonatomic, strong) UITextField *testTF;

//获取验证码按钮
@property (nonatomic, strong) UIButton *getTestCode;

//登陆按钮
@property (nonatomic, strong) UIButton *Login;




@end

@implementation WWYRetrieveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor lightGrayColor];
    // Do any additional setup after loading the view.
    [self setupNavi];
    [self setupLayout];
}

- (void)setupNavi{
    //修改title属性
    self.title = @"修改密码";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:MainTextGreenColor};
    self.navigationController.navigationBar.tintColor = MainTextGreenColor;
    
    //去掉返回文字, 保留返回图标
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    //修改返回键颜色
    self.navigationItem.backBarButtonItem.tintColor = MainTextGreenColor;
}

- (void)setupLayout{
    //背景布局(整体布局)
    _numBack = [UIView new];
    _numBack.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_numBack];
    
    _passWordBack = [UIView new];
    _passWordBack.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_passWordBack];
    
    _checkBack = [UIView new];
    _checkBack.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_checkBack];
    
    _testBack = [UIView new];
    _testBack.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_testBack];
    
    //背景的高度是四十
    [_numBack mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(KTopHeight + 30);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.height.mas_equalTo(40);
    }];
    
    [_passWordBack mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_numBack.mas_bottom).offset(2);
        make.left.equalTo(_numBack);
        make.size.equalTo(_numBack);
    }];
    
    [_checkBack mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_passWordBack.mas_bottom).offset(2);
        make.left.equalTo(_passWordBack);
        make.size.equalTo(_passWordBack);
    }];
    
    [_testBack mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_checkBack.mas_bottom).offset(2);
        make.left.equalTo(_checkBack);
        make.size.equalTo(_checkBack);
    }];
    
    //手机号布局
    _phoneNum = [UILabel  new];
    _phoneNum.text = @"手机号";
    _phoneNum.textAlignment = NSTextAlignmentCenter;
    [_numBack addSubview:_phoneNum];
    [_phoneNum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.equalTo(_numBack);
        make.height.equalTo(_numBack);
        make.width.mas_equalTo(WLABELWIDTH);
        
    }];
    
    //手机号文本框布局
    _numTF = [UITextField new];
    _numTF.keyboardType = UIKeyboardTypeNumberPad;
    _numTF.placeholder = @"请输入手机号";
    [_numBack addSubview:_numTF];
    [_numTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.equalTo(_phoneNum.mas_right);
        make.right.mas_equalTo(0);
        make.height.equalTo(_phoneNum);
    }];
    
    //密码布局
    _passWord = [UILabel new];
    _passWord.text = @"新密码";
    _passWord.textAlignment = NSTextAlignmentCenter;
    [_passWordBack addSubview:_passWord];
    [_passWord mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.left.equalTo(0);
        make.height.equalTo(_passWordBack);
        make.width.mas_equalTo(WLABELWIDTH);
    }];
    
    //密码文本框布局
    _passWordTF = [UITextField new];
    _passWordTF.placeholder = @"请输入您的新密码";
    [_passWordBack addSubview:_passWordTF];
    [_passWordTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.equalTo(_passWord.mas_right);
        make.right.mas_equalTo(0);
        make.height.equalTo(_passWordBack);
    }];
    
    //确认密码布局
    _checkPW = [UILabel new];
    _checkPW.text = @"确认密码";
    _checkPW.textAlignment = NSTextAlignmentCenter;
    [_checkBack addSubview:_checkPW];
    [_checkPW mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(WLABELWIDTH);
        make.height.equalTo(_checkBack);
        
    }];
    
    //确认密码文本编辑框布局
    _checkTF = [UITextField new];
    _checkTF.placeholder = @"请再次输入您的新密码";
    [_checkBack addSubview:_checkTF];
    [_checkTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.equalTo(_checkPW.mas_right);
        make.right.mas_equalTo(0);
        make.height.equalTo(_checkBack);
    }];
    
    //验证码布局
    _testCode = [UILabel new];
    _testCode.text = @"验证码";
    _testCode.textAlignment = NSTextAlignmentCenter;
    [_testBack addSubview:_testCode];
    [_testCode mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(WLABELWIDTH);
        make.height.equalTo(_testBack);
    }];
    
    //验证码文本框
    _testTF = [UITextField new];
    _testTF.placeholder = @"请输入验证码";
    [_testBack addSubview:_testTF];
    [_testTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(_testCode.mas_right);
        make.right.mas_equalTo(-100);
        make.height.equalTo(_testBack);
    }];
    
    //获取验证码按钮
    _getTestCode = [UIButton buttonWithType:UIButtonTypeCustom];
    _getTestCode.backgroundColor = [UIColor lightGrayColor];
    [_getTestCode setTitle:@"获取验证码" forState:UIControlStateNormal];
    _getTestCode.titleLabel.font = [UIFont systemFontOfSize:13];
    [_getTestCode addTarget:self action:@selector(getTestCodeAction:) forControlEvents:UIControlEventTouchUpInside];
    [_testBack addSubview:_getTestCode];
    
    [_getTestCode mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(5);
        make.left.equalTo(_testTF.mas_right);
        make.right.equalTo(-5);
        make.height.equalTo(_testBack.mas_height).offset(-10);
    }];
    
    //登陆按钮
    _Login = [UIButton buttonWithType:UIButtonTypeSystem];
    [_Login setTitle:@"登录" forState:UIControlStateNormal];
    [_Login setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_Login addTarget:self action:@selector(loginAction:) forControlEvents:UIControlEventTouchUpInside];
    _Login.backgroundColor = MainBtnColor;
    [self.view addSubview:_Login];
    [_Login mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_testBack.mas_bottom).offset(30);
        make.left.mas_equalTo(20);
        make.right.mas_equalTo(-20);
        make.height.mas_equalTo(_testBack);
    }];
    
}

//获取验证码方法
- (void)getTestCodeAction:(UIButton *)btn{
    //获取短信
    btn.enabled = NO; //点击设置为不可选
    [self countDown];
    if ([self checkUserInformation]) {
        [self getMassage];
        
    }
}

// 验证用户信息合法
- (BOOL)checkUserInformation{
    if([_numTF.text isEqualToString:_checkTF.text] || [self isLegalPhoneNum]) {
        
    }
    return NO;
}
// 验证电话号码合法
- (BOOL)isLegalPhoneNum{
    return NO;
}
// 验证俩次输入密码一致
- (BOOL)isEqualPassword{
    if ([_passWordTF.text isEqualToString:_checkTF.text]){
        return YES;
    }else{
        //提示框
        
        
        return NO;
    }
}
// 倒计时方法
- (void)countDown{
    __block int timeout = 4; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                _getTestCode.enabled = YES; //设置为可选
                [_getTestCode setTitle:@"重新获取验证码" forState:UIControlStateNormal];
                
            });}else{
                //int minutes = timeout / 60;
                //int seconds = timeout % 60;
                NSString *strTime = [NSString stringWithFormat:@"%.2d's后重新获取", timeout];
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [_getTestCode setTitle:strTime forState:UIControlStateNormal];
                    
                });
                timeout--;
            }
    });
    dispatch_resume(_timer);
}
// 获取短息验证码
- (void)getMassage{
}

// 登录

// 设置制定角圆角
- (void)setMaskToView:(UIView *)view byRoundingCorners:(UIRectCorner)corner{
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(5.0f, 5.0f)];
    
    CAShapeLayer *shape = [[CAShapeLayer alloc] init];
    [shape setPath:path.CGPath];
    
    view.layer.mask = shape;
    
}


- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self setMaskToView:_numBack byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight];
    [self setMaskToView:_testBack byRoundingCorners:UIRectCornerBottomLeft |UIRectCornerBottomRight];
    [self setMaskToView:_getTestCode byRoundingCorners:UIRectCornerAllCorners];

}


- (void)backBtnAction:(UIBarButtonItem *)btn{
    [self.navigationController popViewControllerAnimated:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
