//
//  WWYLogInViewController.m
//  YunboService2016
//
//  Created by Daydream on 16/3/2.
//  Copyright © 2016年 CarService. All rights reserved.
//

#import "WWYLogInViewController.h"
#import "YHJHelp.h"
#import "UserDefautHead.h"
#import "YHNavgationController.h"
#import "WWYRetrieveViewController.h"


#define WLABELWIDTH KScreenWidth / 4


@interface WWYLogInViewController (){
    YHNavgationController *Navi;
}


@property (nonatomic, strong) UILabel *phoneNum;
@property (nonatomic, strong) UITextField *numberTF;
@property (nonatomic, strong) UIView *numberBack;
@property (nonatomic, strong) UILabel *passWord;
@property (nonatomic, strong) UITextField *wordTF;
@property (nonatomic, strong) UIView *passBack;
@property (nonatomic, strong) UIButton *loginBtn;
@property (nonatomic, strong) UIButton *retrieveBtn;




@end

@implementation WWYLogInViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self naviConfig];
    [self setUpLayout];
}

- (void)naviConfig{
    #warning (基类的NavigationBar给隐藏了....)
    self.navigationController.navigationBarHidden = NO;
    self.title = @"用户登录";
    //修改Navigationbar的title
    UIColor * color = MainTextGreenColor;
    NSDictionary * dict=[NSDictionary dictionaryWithObject:color forKey:NSForegroundColorAttributeName];
    self.navigationController.navigationBar.titleTextAttributes = dict;
}

- (void)setUpLayout{
    self.view.backgroundColor = [UIColor lightGrayColor];
    
    //电话号圆角背景
    _numberBack = [UIView new];
    _numberBack.backgroundColor = [UIColor whiteColor];
    
        [self.view addSubview:_numberBack];
    
    //密码圆角背景
    _passBack = [UIView new];
    _passBack.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_passBack];
    
    //背景相对布局, 背景的高度是40;
    [_numberBack mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(KTopHeight +30);
        make.left.equalTo(10);
        make.right.equalTo(-10);
        make.height.equalTo(40);
        
    }];
    [_passBack mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_numberBack.mas_bottom).offset(2);
        make.left.equalTo(_numberBack);
        make.size.equalTo(_numberBack);
    }];
    
    //填写手机号行布局
    _phoneNum = [UILabel new];
    _phoneNum.text = @"手机号码";
    _phoneNum.textAlignment = NSTextAlignmentCenter;
    _phoneNum.backgroundColor = [UIColor whiteColor];
    [self.numberBack addSubview:_phoneNum];
    
    [_phoneNum mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(3);
        make.top.mas_equalTo(0);
        make.width.mas_equalTo(WLABELWIDTH);
        make.height.equalTo( _numberBack.mas_height);
    }];
    
    //手机号文本框布局
    _numberTF = [UITextField new];
    _numberTF.borderStyle = UITextBorderStyleNone;
    _numberTF.keyboardType = UIKeyboardTypeNumberPad;
    _numberTF.placeholder = @"请输入手机号码";
    _numberTF.backgroundColor = [UIColor whiteColor];
    [self.numberBack addSubview:_numberTF];
    
    [_numberTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(3);
        make.top.mas_equalTo(0);
        make.left.equalTo(_phoneNum.mas_right);
        make.height.equalTo(_phoneNum);
        
    }];

    //填写密码行布局
    _passWord = [UILabel new];
    _passWord.text = @"密码";
    _passWord.textAlignment = NSTextAlignmentCenter;
    _passWord.backgroundColor = [UIColor whiteColor];
     [self.passBack addSubview:_passWord];
    [_passWord mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(3);
        make.top.equalTo(0);
        make.size.equalTo(_phoneNum);
    }];

    //密码文本框布局
    _wordTF = [UITextField new];
    _wordTF.placeholder = @"请输入您的登录密码";
    _wordTF.secureTextEntry = YES;
    _wordTF.backgroundColor = [UIColor whiteColor];
    _wordTF.borderStyle = UITextBorderStyleNone;
    [self.passBack addSubview:_wordTF];
    [_wordTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_passWord.mas_right);
        make.right.mas_equalTo(0);
        make.top.equalTo(_passWord);
        make.height.equalTo(_passWord);
        
    }];
    
    
    //登录按钮
    _loginBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [_loginBtn setTitle:@"登录" forState:UIControlStateNormal];
    [_loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _loginBtn.backgroundColor = MainBtnColor;
    [_loginBtn addTarget:self action:@selector(WWYLogin:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_loginBtn];
    
    [_loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_passBack.mas_bottom).offset(30);
        make.left.equalTo(_passBack).offset(10);
        make.right.equalTo(_passBack).offset(-10);
        make.height.equalTo(_passBack);

    }];
    
    //找回密码按钮
    _retrieveBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [_retrieveBtn setTitle:@"忘记密码,请点击这里!" forState:UIControlStateNormal];
    _retrieveBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [_retrieveBtn addTarget:self action:@selector(retrieveBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_retrieveBtn];
    
    [_retrieveBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_loginBtn.mas_bottom);
        make.centerX.equalTo(_loginBtn);
        make.size.equalTo(_loginBtn);
    }];
    
    
}

// 登陆按钮方法
- (void)WWYLogin:(UIButton *)btn{
    NSLog(@"执行登录验证");
    
    if ([self isMobileNumber:_numberTF.text]){
    //    NSDictionary *dic = @{@"service":@"Cleaner.login", @"mobile":_numberTF.text, @"passwd":_wordTF};
        NSDictionary *dic = @{@"service":@"Cleaner.login", @"mobile":@"13567168362", @"passwd":@"123456"};
        [HTTPManager getDataWithUrl:MainAPI andPostParameters:dic andBlocks:^(NSDictionary *result) {
            NSNumber *code = [result objectForKey:@"code"];
            NSLog(@"result %@", result);
            
            if ([code isEqualToNumber:@0]) {
                [YHJHelp showMessage:[result objectForKey:@"message"]];
                [[NSUserDefaults standardUserDefaults] setObject:[result valueForKey:@"serverId"] forKey:USERID];
                _loginBlock();

            }else{
                [YHJHelp showMessage:[result objectForKey:@"message"]];
            }
        }];
    }else{
        [YHJHelp showMessage:@"手机号码不正确"];
    }
}

// 登录成功回调方法
- (void)actionWhenLoginSucceed:(LoginBlock )block{
    self.loginBlock = block;
}

// 验证电话号码
- (BOOL)isMobileNumber:(NSString *)mobileNum
{
    /**
     * 手机号码
     * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     * 联通：130,131,132,152,155,156,185,186
     * 电信：133,1349,153,180,189
     */
    NSString * MOBILE = @"^1(3[0-9]|5[0-35-9]|8[025-9])\\d{8}$";
    /**
     10 * 中国移动：China Mobile
     11 * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     12 */
    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d)\\d{7}$";
    /**
     15 * 中国联通：China Unicom
     16 * 130,131,132,152,155,156,185,186
     17 */
    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
    /**
     20 * 中国电信：China Telecom
     21 * 133,1349,153,180,189
     22 */
    NSString * CT = @"^1((33|53|8[09])[0-9]|349)\\d{7}$";
    /**
     25 * 大陆地区固话及小灵通
     26 * 区号：010,020,021,022,023,024,025,027,028,029
     27 * 号码：七位或八位
     28 */
    // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (([regextestmobile evaluateWithObject:mobileNum] == YES)
        || ([regextestcm evaluateWithObject:mobileNum] == YES)
        || ([regextestct evaluateWithObject:mobileNum] == YES)
        || ([regextestcu evaluateWithObject:mobileNum] == YES))
    {
        return YES;
    }
    else
    {
        return NO;
    }
}


// 跳转找回密码界面
- (void)retrieveBtnAction:(UIButton *)btn{
    WWYRetrieveViewController *retrieve = [[WWYRetrieveViewController alloc] init];
    [self.navigationController pushViewController:retrieve animated:YES];
}

// 在视图布局完成之后, 添加圆角
- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self setMaskTo:_numberBack byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight];
    [self setMaskTo:_passBack byRoundingCorners:UIRectCornerBottomRight | UIRectCornerBottomLeft];
}
// 设置制定圆角
-(void) setMaskTo:(UIView*)view byRoundingCorners:(UIRectCorner)corners
{
    // bounds参数需要有值之后再传
    UIBezierPath* rounded = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:corners cornerRadii:CGSizeMake(5.0f, 5.0f)];

    CAShapeLayer* shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];

    view.layer.mask = shape;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
