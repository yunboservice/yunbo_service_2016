//
//  WWYLogInViewController.h
//  YunboService2016
//
//  Created by Daydream on 16/3/2.
//  Copyright © 2016年 CarService. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YHBaseViewController.h"


typedef void(^LoginBlock)();

@interface WWYLogInViewController : YHBaseViewController

@property (nonatomic, copy) LoginBlock loginBlock;

- (void)actionWhenLoginSucceed:(LoginBlock)block;

@end
