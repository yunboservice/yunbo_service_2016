//
//  YHNavigationBar.m
//  yunbo2016
//
//  Created by apple on 15/10/10.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import "YHNavigationBar.h"
@implementation YHNavigationBar

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = kNavigationBarColor;
        self.dynamic = NO;
        self.blurEnabled = NO;
        self.blurRadius = 25;
        self.tintColor = kNavigationBarColor;
        self.updateInterval = 1.0 / 10;
      
    }
    return self;
}

- (instancetype)init {
    return [self initWithFrame:CGRectZero];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
