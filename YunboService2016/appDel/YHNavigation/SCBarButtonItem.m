//
//  YHNavigationBar.m
//  yunbo2016
//
//  Created by apple on 15/10/10.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import "SCBarButtonItem.h"

@interface SCBarButtonItem ()

@property (nonatomic, strong) UIImage *buttonImage;
@property (nonatomic, strong) UILabel *badgeLabel;

@property (nonatomic, copy) void (^actionBlock)(id);

@end

@implementation SCBarButtonItem


- (instancetype)initWithTitle:(NSString *)title style:(SCBarButtonItemStyle)style handler:(void (^)(id sender))action {

    if (self = [super init]) {
		
        UIButton *button = [[UIButton alloc] init];
        [button setTitle:title forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont systemFontOfSize:KNavigationBarItemTitleFont]];
        [button setTitleColor:kNavigationBarTintColor forState:UIControlStateNormal];
        [button sizeToFit];
        button.height = KNavigationBarHeight;
        button.width += 30;
        button.centerY = KStatusBarHeight + KNavigationBarHeight / 2.0;
        button.x = 0;
		_button = button;
        _view = button;
		if (style == SCBarButtonItemStyleBordered) {
			button.layer.cornerRadius = 4.0f;
			button.layer.borderColor = [UIColor whiteColor].CGColor;
			button.layer.borderWidth = 0.5f;
			button.layer.masksToBounds = YES;
		}

        _actionBlock = action;

        [button addTarget:self action:@selector(handleTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [button addTarget:self action:@selector(handleTouchDown:) forControlEvents:UIControlEventTouchDown];
        [button addTarget:self action:@selector(handleTouchUp:) forControlEvents:UIControlEventTouchCancel|UIControlEventTouchUpOutside|UIControlEventTouchDragOutside];
		[button mas_makeConstraints:^(MASConstraintMaker *make) {
			make.height.greaterThanOrEqualTo(@32);
			make.width.greaterThanOrEqualTo(@48);
		}];
    }

    return self;
}

- (instancetype)initWithImage:(UIImage *)image style:(SCBarButtonItemStyle)style handler:(void (^)(id sender))action {

    if (self = [super init]) {

        _buttonImage = image;

        UIButton *button = [[UIButton alloc] init];
        [button setImage:image forState:UIControlStateNormal];
        [button setImage:image forState:UIControlStateHighlighted];
        [button sizeToFit];
        button.centerY = KStatusBarHeight + KNavigationBarHeight / 2.0;
		_button = button;
        _view = button;
        _actionBlock = action;

        [button addTarget:self action:@selector(handleTouchUpInside:) forControlEvents:UIControlEventTouchUpInside];
        [button addTarget:self action:@selector(handleTouchDown:) forControlEvents:UIControlEventTouchDown];
		[button addTarget:self action:@selector(handleTouchUp:) forControlEvents:UIControlEventTouchCancel|UIControlEventTouchUpOutside|UIControlEventTouchDragOutside];
		[button mas_makeConstraints:^(MASConstraintMaker *make) {
			make.height.greaterThanOrEqualTo(@35);
			make.width.greaterThanOrEqualTo(@30);
		}];
    }

    return self;
}

- (instancetype)initWithCustomView:(UIView *)view{
	if (self = [super init]) {
		view.centerY = KStatusBarHeight + KNavigationBarHeight / 2.0;
		_button = (UIButton *)view;
		_view = view;
		[view mas_makeConstraints:^(MASConstraintMaker *make) {
			make.height.lessThanOrEqualTo(@38);
			make.width.lessThanOrEqualTo(@40);
		}];
	}
	
	return self;
}

- (void)setEnabled:(BOOL)enabled {
    _enabled = enabled;

    if (enabled) {
        _view.userInteractionEnabled = YES;
        _view.alpha = 1.0;
    } else {
        _view.userInteractionEnabled = NO;
        _view.alpha = 0.3;
    }

}

#pragma mark - Private Methods

- (void)handleTouchUpInside:(UIButton *)button {
	if (_actionBlock) {
		_actionBlock(button);
	}
    [UIView animateWithDuration:0.2 animations:^{
        button.alpha = 1.0;
    }];

}

- (void)handleTouchDown:(UIButton *)button {

    button.alpha = 0.3;

}

- (void)handleTouchUp:(UIButton *)button {

    [UIView animateWithDuration:0.3 animations:^{
        button.alpha = 1.0;
    }];

}

@end
