//
//  YHNavigationItem.h
//  yunbo2016
//
//  Created by apple on 15/10/13.
//  Copyright © 2015年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>
@class SCBarButtonItem;


@interface YHNavigationItem : NSObject
@property (nonatomic, strong  ) SCBarButtonItem *leftBarButtonItem;
@property (nonatomic, strong  ) SCBarButtonItem *rightBarButtonItem;
@property (nonatomic, copy    ) NSString        *title;
@property (nonatomic, strong  ) UIView          *titleView;

@end
