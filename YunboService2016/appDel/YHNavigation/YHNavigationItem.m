//
//  YHNavigationItem.m
//  yunbo2016
//
//  Created by apple on 15/10/13.
//  Copyright © 2015年 apple. All rights reserved.
//

#import "YHNavigationItem.h"
#import "SCBarButtonItem.h"
#import "UIViewController+NavigationConfig.h"

static NSInteger const WRightPedding = -8;
@interface YHNavigationItem ()

@property (nonatomic, strong, readwrite) UILabel *titleLabel;
@property (nonatomic, assign) UIViewController *sc_viewController;

@end

@implementation YHNavigationItem


- (void)setTitle:(NSString *)title {
    
    _title = title;
    
    if (!title) {
        _titleLabel.text = @"";
        return;
    }
    
    if ([title isEqualToString:_titleLabel.text]) {
        return;
    }
    
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        [_titleLabel sizeToFit];
        [_titleLabel setFont:[UIFont boldSystemFontOfSize:KNavigationBarTitleFont]];
        //修改过了
        [_titleLabel setTextColor:MainTextColor];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
        [_sc_viewController.sc_navigationBar addSubview:_titleLabel];
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.lessThanOrEqualTo(KScreenWidth - 100);
            make.centerX.equalTo(_sc_viewController.sc_navigationBar.mas_centerX);
            make.centerY.equalTo(_sc_viewController.sc_navigationBar.mas_centerY).offset(KStatusBarHeight / 2);
        }];
        _titleView = _titleLabel;
    }
    
    _titleLabel.text = title;
}

- (void)setLeftBarButtonItem:(SCBarButtonItem *)leftBarButtonItem {
    if (_sc_viewController) {
        [_leftBarButtonItem.view removeFromSuperview];
        [_sc_viewController.sc_navigationBar addSubview:leftBarButtonItem.view];
        [leftBarButtonItem.view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@10);
            make.centerY.equalTo(_sc_viewController.sc_navigationBar.mas_centerY).offset(KStatusBarHeight / 2);
        }];
    }
    
    _leftBarButtonItem = leftBarButtonItem;
}

- (void)setRightBarButtonItem:(SCBarButtonItem *)rightBarButtonItem {
    
    if (_sc_viewController) {
        [_rightBarButtonItem.view removeFromSuperview];
        [_sc_viewController.sc_navigationBar addSubview:rightBarButtonItem.view];
        [rightBarButtonItem.view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(WRightPedding);
            make.centerY.equalTo(_sc_viewController.sc_navigationBar.mas_centerY).offset(KStatusBarHeight / 2 );
        }];
    }
    
    _rightBarButtonItem = rightBarButtonItem;
}

- (void)setTitleView:(UIView *)titleView {
    [_titleLabel removeFromSuperview];
    _titleLabel = nil;
    _title = nil;
    CGSize size = titleView.frame.size;
    if (_sc_viewController) {
        [_sc_viewController.sc_navigationBar addSubview:titleView];
        [titleView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_sc_viewController.sc_navigationBar.mas_centerY).offset(KStatusBarHeight / 2);
            make.centerX.equalTo(0);
            make.size.equalTo(size);
        }];
    }
    _titleView = titleView;
}




@end
