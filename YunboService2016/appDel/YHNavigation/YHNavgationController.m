//
//  YHNavgationController.m
//  yunbo2016
//
//  Created by apple on 15/10/12.
//  Copyright © 2015年 apple. All rights reserved.
//

#import "YHNavgationController.h"
#import "UIViewController+NavigationConfig.h"
#import "YHNavigationBar.h"
#import "YHNavigationItem.h"

@interface YHNavgationController ()<UINavigationControllerDelegate>

@end

@implementation YHNavgationController

- (void)viewDidLoad {
      [super viewDidLoad];
     self.navigationBarHidden = YES;
     super.delegate = self;
    
   
}

#pragma mark - Push & Pop
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    
    [self configureNavigationBarForViewController:viewController];
    [super pushViewController:viewController animated:animated];
}

- (void)navigationController:(UINavigationController *)navigationController
       didShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animate {
    [viewController.view bringSubviewToFront:viewController.sc_navigationBar];
}




- (void)configureNavigationBarForViewController:(UIViewController *)viewController {
    
    if (!viewController.sc_navigationItem) {
        YHNavigationItem *navigationItem = [[YHNavigationItem alloc] init];
        [navigationItem setValue:viewController forKey:@"_sc_viewController"];
        viewController.sc_navigationItem = navigationItem;
    }
    if (!viewController.sc_navigationBar) {
        viewController.sc_navigationBar = [[YHNavigationBar alloc] init];
    }
    if (!viewController.sc_navigationItem.leftBarButtonItem && !viewController.isRootVC) {
        viewController.sc_navigationItem.leftBarButtonItem = [viewController createBackItem];
    }
}

@end
