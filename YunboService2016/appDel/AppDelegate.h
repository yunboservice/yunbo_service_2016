//
//  AppDelegate.h
//  YunboService2016
//
//  Created by apple on 3/2/16.
//  Copyright © 2016 CarService. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

#define appDelegate
@property (strong, nonatomic) NSString *callid;

+(AppDelegate*)shareInstance;
-(void)updateSoftAlertViewShow:(NSString*)message isForceUpdate:(BOOL)isForce;
-(void)toast:(NSString*)message;



@end
 
