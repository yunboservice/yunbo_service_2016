//
//  main.m
//  YunboService2016
//
//  Created by apple on 3/2/16.
//  Copyright © 2016 CarService. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
