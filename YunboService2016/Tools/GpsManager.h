//
//  GpsManager.h
//  CarService
//
//  Created by apple on 15/5/21.
//  Copyright (c) 2015年 fenglun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BaiduMapAPI_Location/BMKLocationComponent.h>
#import <BaiduMapAPI_Search/BMKSearchComponent.h>
//定位
typedef void(^BlockLocation)(BMKUserLocation *location);
//编码
typedef void(^BlockReverseGeoCode)(BMKReverseGeoCodeResult*result ,BMKSearchErrorCode error);
typedef void(^Block_AllInfo)(BMKUserLocation *location,BMKReverseGeoCodeResult *result);


@interface GpsManager : NSObject<BMKGeoCodeSearchDelegate,BMKLocationServiceDelegate>

@property (nonatomic ,strong)  BMKGeoCodeSearch   *geocodesearch;  //百度 地理编码
@property (nonatomic ,strong)  BMKLocationService *locService;        //百度 定位
@property (nonatomic ,strong)  BMKReverseGeoCodeOption *reverseGeoCodeOption;

@property (nonatomic, copy) BlockLocation Locationbolck;
@property (nonatomic, copy) BlockReverseGeoCode ReverseGeoCodebolck;
@property (nonatomic ,copy) Block_AllInfo allInfoBlock;



- (void) start;
- (void) stop;

+ (instancetype) sharedGpsManager;
//- (void) getUserLocationWashCarBolck:(BlockWashCar)bolck;
//获取定位
-(void)getLocationOnceBolck:(BlockLocation)block;
//地理编码
-(void)getReverseGeoCode:(CLLocationCoordinate2D)coordinaote andBolck:(BlockReverseGeoCode)block;
-(void)getAllInfoOnceBlock:(Block_AllInfo) allInfoBlock;

@end
