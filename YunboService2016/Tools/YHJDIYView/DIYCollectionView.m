//
//  DIYCollectionView.m
//  yunbo2016
//
//  Created by Hanks on 15/12/30.
//  Copyright © 2015年 apple. All rights reserved.
//

#import "DIYCollectionView.h"
#import "DIYCollectioncell.h"

#define ITMEH    35
#define ITMEW    ITMEH

@interface DIYCollectionView ()<UICollectionViewDataSource,UICollectionViewDelegate>
{
    UIButton *_tempBtn;
}

@property (nonatomic  ,strong) NSArray       *titleArr;
@property (nonatomic , strong) UICollectionView *cv;


@end

@implementation DIYCollectionView

- (instancetype)init
{
    self = [super init];
    if (self) {
        _titleArr = [self configeData];
        [self makeCollectionView];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self makeCollectionView];
    }
    return self;
}


-(instancetype)initWithTitle:(NSArray*)titleArr
{
    if (self = [super init]) {
        _titleArr   =  titleArr;
        [self makeCollectionView];
    }
    
    return self;
}


-(NSArray *)configeData
{
    return @[@"川",@"鄂",@"桂",@"甘",@"贵",@"沪",@"黑",@"冀",@"津",@"京",@"吉",@"晋",@"辽",@"鲁",@"蒙",@"闽",@"宁",@"琼",@"黔",@"青",@"苏",@"陕",@"晥",@"湘",@"新",@"云",@"豫",@"渝",@"浙",@"粤",@"港",@"澳",@"台",@"其他"];
    
}

-(void)makeCollectionView
{
    if (!_cv) {
        // 布局方式
        UICollectionViewFlowLayout *layOut = [[UICollectionViewFlowLayout alloc]init];
        //设置item 大小
        [layOut setItemSize:CGSizeMake(ITMEW, ITMEH)];
        
        layOut.minimumInteritemSpacing = 3;
        layOut.minimumLineSpacing = 5;
//        layOut.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        
        //设置排列方式
        //        layOut.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layOut.scrollDirection = UICollectionViewScrollDirectionVertical;
        _cv = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layOut];
        
        _cv.delegate = self;
        _cv.dataSource   = self;
        _cv.backgroundColor  = [UIColor clearColor];
        _cv.opaque  =  YES ;
        
        //注册Cell
        [_cv registerClass:[DIYCollectioncell class ] forCellWithReuseIdentifier:@"Cell"];
        
        [self addSubview:_cv];
        
        [_cv mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(UIEdgeInsetsMake(6, 0, 0, 0));
        }];
    }
    
    
}




#pragma mark ----collection method
// 必须实现的代理
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return  _titleArr.count;
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{   // 开始找 cell
    static NSString *cellName = @"Cell";
    DIYCollectioncell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellName forIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.contentView.opaque = YES;
    [cell.titleBtn addTarget:self action:@selector(titleBtnDown:) forControlEvents:UIControlEventTouchUpInside];
    [cell configCollectionCellWithTitle:_titleArr[indexPath.row]];
    
    
    return cell;
    
}

-(void)titleBtnDown:(UIButton *)sender
{
    NSArray *cellArr = [_cv visibleCells ];
    for (DIYCollectioncell *cell in cellArr) {
        cell.titleBtn.selected = NO;
        cell.titleBtn.backgroundColor = [UIColor whiteColor];
    }
    sender.selected = YES;
    sender.backgroundColor = MainBtnColor;
    if ([_delegate respondsToSelector:@selector(selectedTitle:andConllection:)]) {
        [_delegate selectedTitle:sender.currentTitle andConllection:self];
    }
}


@end
