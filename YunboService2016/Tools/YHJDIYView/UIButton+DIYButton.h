//
//  UIButton+DIYButton.h
//  yunbo2016
//
//  Created by apple on 15/11/30.
//  Copyright © 2015年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    logInType,
    washType,
} CommonlyWithButtonType;

@interface UIButton (DIYButton)
/**
 *  初始化
 *
 *  @param imageName Button上的Image  可为nil   （依赖性0）
 *  @param title     Button上的标题
 *  @param color     Button上的背景色
 *
 *  @return Button＊
 */
+(UIButton *)buttonWithImage:(NSString *)imageName andTitel:(NSString *)title andBackColor:(UIColor *)color;

/**
 *  初始化
 *
 *  @param status 枚举 根据枚举 来创建Button（依赖性＊＊）
 *
 *  @return Button＊
 */
+ (instancetype)initCommonlyWithStatus:(CommonlyWithButtonType)status;

@end
