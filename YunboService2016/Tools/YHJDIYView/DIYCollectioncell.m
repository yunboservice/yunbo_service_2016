//
//  DIYCollectioncell.m
//  yunbo2016
//
//  Created by Hanks on 15/12/30.
//  Copyright © 2015年 apple. All rights reserved.
//

#import "DIYCollectioncell.h"


@implementation DIYCollectioncell





- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _titleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _titleBtn.backgroundColor = [UIColor whiteColor];
        [_titleBtn setTitleColor:kNavigationBarColor forState:UIControlStateNormal];
        [_titleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        _titleBtn.layer.cornerRadius = 2;
        _titleBtn.layer.masksToBounds = YES;
        _titleBtn.titleLabel.font = [UIFont systemFontOfSize:16];
       [_titleBtn addTarget:self action:@selector(titleBtnDown:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_titleBtn];
        
        [_titleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(0);
        }];
    }
    
    return self;
}

-(void)configCollectionCellWithTitle:(NSString *)title
{
    [_titleBtn setTitle:title forState:UIControlStateNormal];
    if (title.length>=2) {
        
        _titleBtn.titleLabel.numberOfLines = 2;
        _titleBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    }
     
}

-(void)titleBtnDown:(UIButton *)sender
{
    sender.selected = !sender.selected;
    if (sender.selected) {
        sender.backgroundColor = MainBtnColor;
    }else
    {
        sender.backgroundColor = [UIColor whiteColor];
    }
}


@end
