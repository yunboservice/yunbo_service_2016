//
//  DIYCollectionView.h
//  yunbo2016
//
//  Created by Hanks on 15/12/30.
//  Copyright © 2015年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DIYCollectionView;
@protocol  DIYCollectionViewDelegate<NSObject>

@optional
-(void)selectedTitle:(NSString *)title andConllection:(DIYCollectionView *)collectionView;

@end
@interface DIYCollectionView : UIView

@property (nonatomic , weak) id <DIYCollectionViewDelegate> delegate;
-(instancetype)initWithTitle:(NSArray*)titleArr;


@end
