//
//  UIButton+DIYButton.m
//  yunbo2016
//
//  Created by apple on 15/11/30.
//  Copyright © 2015年 apple. All rights reserved.
//

#import "UIButton+DIYButton.h"

@implementation UIButton (DIYButton)


+(UIButton *)buttonWithImage:(NSString *)imageName andTitel:(NSString *)title andBackColor:(UIColor *)color
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor = color;
    button.layer.cornerRadius = 4;
    button.layer.masksToBounds = YES;
    //1.image title 2.image 3.title 4.nil
    if (nil != imageName && nil != title) {
        [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateHighlighted];
        button.imageEdgeInsets =UIEdgeInsetsMake(0, 0, 0, 15);

        [button setTitle:title forState:UIControlStateNormal];
        [button setTitleColor:MainBtnUColor forState:UIControlStateHighlighted];
        button.titleLabel.font = [UIFont systemFontOfSize:18];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    }else if(nil == imageName && nil != title){
        [button setTitle:title forState:UIControlStateNormal];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [button setTitleColor:MainBtnUColor forState:UIControlStateHighlighted];
        button.titleLabel.font = [UIFont systemFontOfSize:18];

    }else if(nil !=imageName && nil == title){
        [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateHighlighted];
    }else{//nil
        
    }
    
    [button addTarget:self action:@selector(btnTouchDown:) forControlEvents:UIControlEventTouchDown];
    [button addTarget:self action:@selector(btnTouchUp:) forControlEvents:UIControlEventTouchUpInside];
    return button;
}


+(instancetype)initCommonlyWithStatus:(CommonlyWithButtonType)status{
    if (status == logInType) {
        return [UIButton buttonWithImage:@"login_login" andTitel:@"登录" andBackColor:MainBtnColor];
    }
    else if (status == washType){
        
    }

    return [[UIButton alloc]init];
}

+(void)btnTouchDown:(UIButton *)send
{
    send.backgroundColor = KColorFromRGB(0x54a131);
    
}


+(void)btnTouchUp:(UIButton *)send
{
    send.backgroundColor = MainBtnColor;
}


@end
