//
//  DIYCollectioncell.h
//  yunbo2016
//
//  Created by Hanks on 15/12/30.
//  Copyright © 2015年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DIYCollectioncell : UICollectionViewCell


@property (nonatomic ,strong) UIButton *titleBtn;


-(void)configCollectionCellWithTitle:(NSString *)title;


@end
