//
//  DIYTipView.m
//  yunbo2016
//
//  Created by Hanks on 16/2/19.
//  Copyright © 2016年 apple. All rights reserved.
//

#import "DIYTipView.h"

@interface DIYTipView   ()


@property (nonatomic , strong) UIButton  *clickBtn;
@property (nonatomic , copy) void(^finishBlock)();

@end

@implementation DIYTipView

-(instancetype)initWithInfo:(NSString *)infoStr andBtnTitle:(NSString *) btnTitleStr andClickBlock:(void(^)()) block
{
    if (self = [super init]) {
        self.backgroundColor = [UIColor clearColor];
        if (block) {
            _finishBlock = block;
        }
        _infoLabel = [[UILabel alloc ]init];
        _infoLabel.textColor = MainTextLightGrayColor;
        _infoLabel.font = Font(14);
        _infoLabel.text = infoStr;
        [_infoLabel sizeToFit];
        [self addSubview:_infoLabel];
        
        [_infoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
           
            if (btnTitleStr) {
                make.centerX.equalTo(0);
                make.top.equalTo(5);
                
            }else
            {
              _infoLabel.font = Font(16);
                make.center.equalTo(0);
            }
            
        }];
        
        [self layoutIfNeeded];
        
        if (btnTitleStr) {
            
            UIButton *clickBtn = [self btnWithTitle:btnTitleStr];
            [clickBtn addTarget:self action:@selector(clickBtnDown) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:clickBtn];
            
            [clickBtn mas_makeConstraints:^(MASConstraintMaker *make) {
               
                make.size.equalTo(CGSizeMake(120, 30));
                make.top.equalTo(_infoLabel.mas_bottom).offset(15);
                make.centerX.equalTo(0);
            }];
        }
        
    }
    
    return self;
}


-(void)clickBtnDown
{
    _finishBlock();
}


-(UIButton *)btnWithTitle:(NSString *)title
{
 
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:MainBtnColor forState:UIControlStateNormal];
    btn.layer.cornerRadius = 4;
    btn.layer.borderColor = MainBtnColor.CGColor;
    btn.layer.borderWidth = 1.0;
    btn.layer.masksToBounds = YES;
    btn.titleLabel.font = Font(16);
    
    return btn;
}


@end
