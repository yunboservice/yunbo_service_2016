//
//  DIYTipView.h
//  yunbo2016
//
//  Created by Hanks on 16/2/19.
//  Copyright © 2016年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DIYTipView : UIView

-(instancetype)initWithInfo:(NSString *)infoStr andBtnTitle:(NSString *) btnTitleStr andClickBlock:(void(^)()) block;
@property (nonatomic , strong) UILabel   *infoLabel;

@end
