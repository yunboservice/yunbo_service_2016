//
//  UILabel+DIYLabel.m
//  yunbo2016
//
//  Created by Hanks on 16/1/21.
//  Copyright © 2016年 apple. All rights reserved.
//

#import "UILabel+DIYLabel.h"

@implementation UILabel (DIYLabel)

+(UILabel *)labelWithText:(NSString*)text andFont:(CGFloat)font andTextColor:(UIColor *)color andTextAlignment:(NSTextAlignment)textAlignment
{
    UILabel *lable = [[UILabel alloc]init];
    [lable sizeToFit];
    lable.text = text;
    lable.font = [UIFont systemFontOfSize:font];
    if (color) {
        lable.textColor = color;
    }else
    {
        lable.textColor = MainTextGrayColor;
    }
    
    if (textAlignment) {
        lable.textAlignment = textAlignment;
    }
    
    return lable;
}

@end
