//
//  UILabel+DIYLabel.h
//  yunbo2016
//
//  Created by Hanks on 16/1/21.
//  Copyright © 2016年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (DIYLabel)

+(UILabel *)labelWithText:(NSString*)text andFont:(CGFloat)font andTextColor:(UIColor *)color andTextAlignment:(NSTextAlignment)textAlignment;

@end
