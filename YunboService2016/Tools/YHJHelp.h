//
//  YHJHelp.h
//  yunbo2016
//
//  Created by apple on 15/10/22.
//  Copyright © 2015年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YHJHelp : NSObject

/**
 *  主程序的window
 */
+ (UIWindow *)mainWindow;

/**
 *  判断程序是否第一次启动或者升级后第一次启动
 */
+ (BOOL)isFirstLoad;

/**
 *  是否接通网络
 */
+ (BOOL)isReachable;
/**
 *  手机型号
 *
 */
+ (NSString*)deviceString;
/**
 *  是否是手机号码
 */
+ (NSString *)valiMobile:(NSString *)mobile;

/**
 *   分享设置
 *
 *  @param controller 传入需要显示的controller对象
 */
+(void)showShareInController:(UIViewController *)controller andShareURL:(NSString *)urlStr andTitle:(NSString *)title andShareText:(NSString *) shareText andShareImage:(UIImage *)image;
/**
 *  检查是否登录
 *
 *  @param controll 监测的controller
 *
 *  @return
 */
+ (BOOL) isLoginAndIsNetValiadWitchController:(UIViewController *)controll;

/**
 *  监测是否有摄像头
 *
 *  @return 
 */
+(BOOL)isCustom;

/**
 *  显示信息 （alter）
 *
 *  @param message 提示信息
 */
+(void)showMessage:(NSString*)message;

/**
 *  修改视图圆角
 *
 *  @param view    需要修改的视图
 *  @param corners 选择修改的角
 *  @param radian  圆角弧度
 */
+(void) setMaskTo:(UIView*)view byRoundingCorners:(UIRectCorner)corners WithRadii:(CGFloat)radian;

/**
 *  Label的自适应文字大小
 *
 *  @param size      一个定长或定高size
 *  @param labelText Label中要显示的文字
 *  @param font      字体
 *
 *  @return 自适应过得size
 */
+(CGSize)labelRectWithSize:(CGSize)size LabelText:(NSString *)labelText Font:(UIFont *)font;



@end
