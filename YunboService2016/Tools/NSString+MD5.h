//
//  NSString+MD5.h
//  TZS
//
//  Created by yandi on 14/12/9.
//  Copyright (c) 2014年 NongFuSpring. All rights reserved.
//

@interface NSString (MD5)

+ (NSString *)md5:(NSString *)originalStr;
+(NSString*)CurrentTime1970;
+(NSString *)userLocWith:(double) uerLog andLat:(double) userLat PublisLocWith:(double) Plog andLat:(double) Plat;
+(NSString*)TimeToCurrentTime:(NSNumber *)time;
+(NSString*)CurrentTimeByStr;
@end
