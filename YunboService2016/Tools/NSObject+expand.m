//
//  NSObject+expand.m
//  yunbo2016
//
//  Created by apple on 15/11/20.
//  Copyright © 2015年 apple. All rights reserved.
//

#import "NSObject+expand.h"
#import <objc/runtime.h>


static char const *DIC = "expandDic";

@implementation NSObject (expand)


-(void)setExpandDic:(NSDictionary *)expandDic
{
    objc_setAssociatedObject(self, DIC, expandDic, OBJC_ASSOCIATION_RETAIN);
}

-(NSDictionary*)expandDic
{
    return objc_getAssociatedObject(self, DIC);
}

@end
