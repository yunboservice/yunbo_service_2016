//
//  NSObject+expand.h
//  yunbo2016
//
//  Created by apple on 15/11/20.
//  Copyright © 2015年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSObject (expand)

@property (nonatomic , strong) NSDictionary *expandDic;

@end
