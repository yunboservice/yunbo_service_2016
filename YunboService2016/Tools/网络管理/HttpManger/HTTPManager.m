//
//  HTTPManager.m
//  yunbo2016
//
//  Created by apple on 15/10/22.
//  Copyright © 2015年 apple. All rights reserved.
//

#import "HTTPManager.h"
#import "AFAppNetClient.h"
#import "YhjRootViewController.h"

#define HTTPKEY       @"9d44ebd7128037652d765b29aee70739"
#define SHOWSERVICE   @"User.applyForInvoice"

@implementation HTTPManager

+ (NSURLSessionDataTask *)getDataWithApiPre:(NSString *)apiPre GetParameters:(NSDictionary *)getParameters andPostParameters:(NSDictionary *)postParameters andBlocks:(void (^)(NSDictionary *result))block{
    
    if (!postParameters) {
        return [[AFAppNetClient sharedClient] GET:apiPre parameters:getParameters success:^(NSURLSessionDataTask *task, NSDictionary *responseObject) {
            NSNumber *ret = [responseObject objectForKey:@"ret"];
            NSDictionary *resultdic = (NSDictionary *)responseObject;
            if ([ret isEqualToNumber:@200]) {
                if (block) {
                    block(resultdic[@"data"]);
                }
            }else if ([ret isEqualToNumber:@401])
            {
                if (NOEmptyStr(KgetUserValueByParaName(USERID))) {
                    //                    [self logout];
                }
            }else
            {
                [YAlertView showAlertViewWithString:responseObject[@"msg"]];
                block(nil);
            }
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            if (error.code == -1001) {
                [YAlertView showAlertViewWithString:@"请求超时,请检查网络"];
            }
        }];
    }else {
        if ([[getParameters objectForKey:@"pics"] count]) {
            return [[AFAppNetClient   sharedClient]POST:apiPre parameters:postParameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                NSArray *imageArr = getParameters[@"pics"];
                for(int i= 0 ; i< imageArr.count; i++){
                    NSData *imageData = imageArr[i];
                    
                    [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"%@[]",KgetUserValueByParaName(USERID)] fileName:[NSString stringWithFormat:@"%d.jpg",i+1] mimeType:@"image/jpeg"];
                }
            } success:^(NSURLSessionDataTask *task, id responseObject) {
                NSNumber *ret = [responseObject objectForKey:@"ret"];
                NSDictionary *resultdic = (NSDictionary *)responseObject;
                if ([ret isEqualToNumber:@200]) {
                    
                    block(resultdic[@"data"]);
                    
                    
                }else if ([ret isEqualToNumber:@401])
                {
                    //                    [self logout];
                }else
                {
                    [YAlertView showAlertViewWithString:responseObject[@"msg"]];
                    block(nil);
                }
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                
                if (error.code == -1001) {
                    [YAlertView showAlertViewWithString:@"请求超时,请检查网络"];
                }
                block(nil);
            }];
        }else{
            return [[AFAppNetClient sharedClient] POST:apiPre parameters:postParameters success:^(NSURLSessionDataTask *task, id responseObject) {
                NSDictionary *resultdic = (NSDictionary *)responseObject;
                if([[postParameters objectForKey:@"service"]
                    isEqualToString:SHOWSERVICE]){
                    NSLog(@"%@",responseObject);
                }
                
                NSNumber *ret = [responseObject objectForKey:@"ret"];
                if ([ret isEqualToNumber: @200]) {
                    
                    if (block) {
                        block(resultdic[@"data"]);
                    }
                } else if ([ret isEqualToNumber:@401])
                {
                    //                    [self logout];
                }
                else
                {
                    block(nil);
                    
                    [YAlertView showAlertViewWithString:responseObject[@"msg"]];
                }
                
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                if (error.code == -1001) {
                    [YAlertView showAlertViewWithString:@"请求超时,请检查网络"];
                }
                block(nil);
            }];
        }
    }
    
}



+ (NSURLSessionDataTask *)getDataWithUrl:(NSString *)urlPath andPostParameters:(NSDictionary *)postPatameters andBlocks:(void (^)(NSDictionary *result))block
{
    
    //    MyLog(@"%@", postPatameters);
    return [self getDataWithApiPre:urlPath GetParameters:nil andPostParameters:[self configCommonPamerDic:postPatameters] andBlocks:block];
}



+(NSURLSessionDataTask *) publishCarCirclWithUrl:(NSString *)urlPath andPostParameters:(NSDictionary *)postParamers andImageDic:(NSDictionary *)imagePatamer andBlocks:(void(^)(NSDictionary *result)) block
{
    return [self getDataWithApiPre:urlPath GetParameters:imagePatamer andPostParameters:[self configCommonPamerDic:postParamers] andBlocks:block];
    
}



+(NSDictionary *)configCommonPamerDic:(NSDictionary *)configDic
{
    if ([[configDic allKeys] containsObject:@"userId"]) {
        NSString *str = [configDic objectForKey:@"userId"];
        if (IsEmptyStr(str)) {
            return nil;
        }
    }
    
    NSMutableDictionary *sendDic = [NSMutableDictionary dictionaryWithDictionary:configDic];
    NSString *appVersion = [NSString stringWithFormat:@"IOS_%@",APPversion];
    [sendDic setObject:appVersion forKey:@"res"];
    [sendDic setObject:[NSString CurrentTime1970] forKey:@"time"];
    [sendDic setObject:HTTPKEY forKey:@"key"];
    NSString *isUserId =[sendDic objectForSafeKey:@"userId"];
    //请求有userid时 需要 拼secret
    if (IsEmptyStr(isUserId)) {//没有userId 这个参数就不需要拼接secret
    }else{
        NSString *resStr =[[NSUserDefaults standardUserDefaults] objectForKey:HCuserSecret];
        if (resStr) {
            [sendDic setObject:resStr forKey:@"secret"];
        }
        NSAssert(resStr!=nil, @"the secretStr is NULL ");
    }
    NSMutableString *resultStr = [[NSMutableString alloc]init];
    NSArray *newKeys = [[sendDic allKeys] sortedArrayUsingSelector:@selector(compare:)];
    for (NSString *key in newKeys) {
        id value =sendDic[key];
        if (![value isKindOfClass:[NSDictionary class]]&&![value isKindOfClass:[NSArray class]])
        {
            NSString *strValue = [NSString stringWithFormat:@"%@",value];
            if (IsEmptyStr(strValue)) {
                [sendDic removeObjectForKey:key];
            }else
            {
                [resultStr appendFormat:@"%@=%@&",key,sendDic[key]];
                
            }
        }
    }
    
    NSString *MD5Str = [NSString md5:[resultStr substringToIndex:resultStr.length-1]];
    
    
    [sendDic setObject:MD5Str forKey:@"sign"];
    [sendDic removeObjectForKey:@"secret"];
    if([[sendDic objectForKey:@"service"]
        isEqualToString:SHOWSERVICE]){
        NSLog(@"sendDic = %@",sendDic);
        NSLog(@"MD5Str = %@",MD5Str);
        NSLog(@"resultStr =%@",resultStr);
    }
    
    return sendDic;
}



#pragma mark  ===退出===

/**
 *  ------------第二代-------------------
 */
//#pragma mark //-第二代-
+ (NSURLSessionDataTask *)getDataWithUrl:(NSString *)urlPath andPostParameters:(NSDictionary *)postPatameters andHub:(HubStyle)hubMode andBlocks:(void (^)(NSDictionary *result))block{
    return [self getDataWithApiPre:urlPath GetParameters:nil andPostParameters:[self configCommonPamerDic:postPatameters] andHub:hubMode andBlocks:block];
}

+ (NSURLSessionDataTask *)getDataWithApiPre:(NSString *)apiPre GetParameters:(NSDictionary *)getParameters andPostParameters:(NSDictionary *)postParameters andHub:(HubStyle)hubMode andBlocks:(void (^)(NSDictionary *result))block{
    
    UIWindow * window = [UIApplication sharedApplication].keyWindow;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:window animated:YES];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [hud hide:YES];
        });
    });
    
    if (!postParameters) {
        return [[AFAppNetClient sharedClient] GET:apiPre parameters:getParameters success:^(NSURLSessionDataTask *task, NSDictionary *responseObject) {
            NSNumber *ret = [responseObject objectForKey:@"ret"];
            NSDictionary *resultdic = (NSDictionary *)responseObject;
            if ([ret isEqualToNumber:@200]) {
                if (block) {
                    block(resultdic[@"data"]);
                }
            }else if ([ret isEqualToNumber:@401])
            {
                if (NOEmptyStr(KgetUserValueByParaName(USERID))) {
                    //[self logout];
                }
            }else
            {
                [YAlertView showAlertViewWithString:responseObject[@"msg"]];
                block(nil);
            }
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            if (error.code == -1001) {
                [YAlertView showAlertViewWithString:@"请求超时,请检查网络"];
            }
        }];
    }else {
        if ([[getParameters objectForKey:@"pics"] count]) {
            return [[AFAppNetClient   sharedClient]POST:apiPre parameters:postParameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                NSArray *imageArr = getParameters[@"pics"];
                for(int i= 0 ; i< imageArr.count; i++){
                    NSData *imageData = imageArr[i];
                    
                    [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"%@[]",KgetUserValueByParaName(USERID)] fileName:[NSString stringWithFormat:@"%d.jpg",i+1] mimeType:@"image/jpeg"];
                }
            } success:^(NSURLSessionDataTask *task, id responseObject) {
                NSNumber *ret = [responseObject objectForKey:@"ret"];
                NSDictionary *resultdic = (NSDictionary *)responseObject;
                if ([ret isEqualToNumber:@200]) {
                    
                    block(resultdic[@"data"]);
                    
                    
                }else if ([ret isEqualToNumber:@401])
                {
                   // [self logout];
                }else
                {
                    [YAlertView showAlertViewWithString:responseObject[@"msg"]];
                    block(nil);
                }
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                
                if (error.code == -1001) {
                    [YAlertView showAlertViewWithString:@"请求超时,请检查网络"];
                }
                block(nil);
            }];
        }else{
            return [[AFAppNetClient sharedClient] POST:apiPre parameters:postParameters success:^(NSURLSessionDataTask *task, id responseObject) {
                NSDictionary *resultdic = (NSDictionary *)responseObject;
                if([[postParameters objectForKey:@"service"]
                    isEqualToString:SHOWSERVICE]){
                    NSLog(@"%@",responseObject);
                }
                
                NSNumber *ret = [responseObject objectForKey:@"ret"];
                if ([ret isEqualToNumber: @200]) {
                    
                    if (block) {
                        block(resultdic[@"data"]);
                    }
                } else if ([ret isEqualToNumber:@401])
                {
                   // [self logout];
                }
                else
                {
                    block(nil);
                    [YAlertView showAlertViewWithString:responseObject[@"msg"]];
                }
                
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                if (error.code == -1001) {
                    [YAlertView showAlertViewWithString:@"请求超时,请检查网络"];
                }
                block(nil);
            }];
        }
    }
    
}


@end
