//
//  GpsManager.m
//  CarService
//
//  Created by apple on 15/5/21.
//  Copyright (c) 2015年 fenglun. All rights reserved.
//

#import "GpsManager.h"

@interface GpsManager ()
{
    BMKUserLocation *_myLocation;
}

@end

@implementation GpsManager

static GpsManager  *_instance;

+ (instancetype)sharedGpsManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[GpsManager alloc] init];
    });
    return _instance;
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        
        _locService = [[BMKLocationService alloc] init];
        _reverseGeoCodeOption = [[BMKReverseGeoCodeOption alloc] init];
        _geocodesearch = [[BMKGeoCodeSearch alloc] init];
    }
    return self;
    
}

-(void)start{
    
    if ([CLLocationManager locationServicesEnabled] &&
        (([CLLocationManager authorizationStatus] != kCLAuthorizationStatusRestricted) &&
         ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied))) {
            _locService.delegate = self;
            [_locService startUserLocationService];
        } else {
            NSString *alertStr = IOS7 ? @"设置－隐私－定位服务" : @"设置－定位服务";
            NSString *message = [NSString stringWithFormat:@"云泊没有获得系统的位置信息权限，无法获得你的位置\n\n(在iPhone的%@中开启)", alertStr];
            UIAlertView *view = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"没有位置信息权限", nil)
                                                           message:NSLocalizedString(message, @"")
                                                          delegate:nil
                                                 cancelButtonTitle:NSLocalizedString(@"确定", nil)
                                                 otherButtonTitles:nil, nil];
            [view show];
        }
    
    
}

-(void)stop
{
    [_locService stopUserLocationService];
    _locService.delegate = nil;
    
}

- (void)didUpdateBMKUserLocation:(BMKUserLocation *)userLocation
{
    _myLocation  = userLocation;
    if (_Locationbolck) {
        _Locationbolck(userLocation);
    }
    if (_allInfoBlock) {
        _geocodesearch.delegate = self;
        _reverseGeoCodeOption.reverseGeoPoint = userLocation.location.coordinate;
        [_geocodesearch reverseGeoCode:_reverseGeoCodeOption];
    }
    
}

- (void)onGetReverseGeoCodeResult:(BMKGeoCodeSearch*)searcher result:(BMKReverseGeoCodeResult*)result errorCode:(BMKSearchErrorCode)error
{
    if (_ReverseGeoCodebolck) {
        _ReverseGeoCodebolck(result, error);
    }
    if (_allInfoBlock) {
        _allInfoBlock(_myLocation,result);
        [self stop];
    }
    _allInfoBlock = nil;
    
    _geocodesearch.delegate = nil;
    
}

//获取定位
-(void)getLocationOnceBolck:(BlockLocation)block{
    _Locationbolck = [block copy];
    [self start];
}
//地理编码
-(void)getReverseGeoCode:(CLLocationCoordinate2D)coordinaote andBolck:(BlockReverseGeoCode)block{
    _ReverseGeoCodebolck = [block copy];
    _geocodesearch.delegate = self;
    _reverseGeoCodeOption.reverseGeoPoint = coordinaote;
    [_geocodesearch reverseGeoCode:_reverseGeoCodeOption];
    
}

-(void)getAllInfoOnceBlock:(Block_AllInfo) allInfoBlock
{
    _allInfoBlock = [allInfoBlock copy];
    [self start];
}


@end
