//
//  YHJHelp.m
//  yunbo2016
//
//  Created by apple on 15/10/22.
//  Copyright © 2015年 apple. All rights reserved.
//

#import "YHJHelp.h"
#import "Reachability.h"
#import "sys/utsname.h"
#import <AVFoundation/AVFoundation.h>
#import "UMSocial.h"
#import "UMSocialWechatHandler.h"
#import "UMSocialSinaHandler.h"
#import "UMSocialQQHandler.h"
//#import "LoginViewController.h"
#import "WWYLogInViewController.h"
#import "YHNavgationController.h"
#import "WFHudView.h"

static NSString *WLastVersion = @"last_run_version_of_application";

@implementation YHJHelp

+ (UIWindow *)mainWindow{
    return [UIApplication sharedApplication].delegate.window;
}


#pragma mark 是否是第一次启动新版本
+ (BOOL) isFirstLoad{
    NSString *currentVersion = [[[NSBundle mainBundle] infoDictionary]
                                objectForKey:(NSString*)kCFBundleVersionKey];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *lastRunVersion = [defaults objectForKey:WLastVersion];
    if (!lastRunVersion) {
        [defaults setObject:currentVersion forKey:WLastVersion];
        return YES;
    }else if (![lastRunVersion isEqualToString:currentVersion]) {
        [defaults setObject:currentVersion forKey:WLastVersion];
        return YES;
    }
    
    return NO;
}



+ (BOOL)isReachable{
    return [[Reachability reachabilityWithHostName:@"www.baidu.com"] isReachable];
}

+ (NSString*)deviceString
{
    // 需要#import "sys/utsname.h"
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *deviceString = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    if ([deviceString isEqualToString:@"iPhone7,1"]) {return @"iPhone6Plus";
    }if ([deviceString isEqualToString:@"iPhone7,2"]) {return @"iPhone6";}
    if ([deviceString isEqualToString:@"iPhone5,1"]||[deviceString isEqualToString:@"iPhone5,2"])    return @"iPhone 5";
    if ([deviceString isEqualToString:@"iPhone6,2"]||[deviceString isEqualToString:@"iPhone6,1"])    return @"iPhone 5S";
    if ([deviceString isEqualToString:@"iPhone5,3"]||[deviceString isEqualToString:@"iPhone5,4"])    return @"iPhone 5C";
    if ([deviceString isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([deviceString isEqualToString:@"iPhone3,1"]||[deviceString isEqualToString:@"iPhone3,2"]||[deviceString isEqualToString:@"iPhone3,3"])    return @"iPhone 4";
    if ([deviceString isEqualToString:@"iPhone3,2"])    return @"Verizon iPhone 4";
    if ([deviceString isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([deviceString isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([deviceString isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([deviceString isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    
    //  MyLog(@"NOTE: Unknown device type: %@", deviceString);
    return deviceString;
}

+ (NSString *)valiMobile:(NSString *)mobile{
    if (mobile.length < 11)
    {
        return @"手机号长度只能是11位";
    }else{
        /**
         * 移动号段正则表达式
         */
        NSString *CM_NUM = @"^((13[4-9])|(147)|(15[0-2,7-9])|(178)|(18[2-4,7-8]))\\d{8}|(1705)\\d{7}$";
        /**
         * 联通号段正则表达式
         */
        NSString *CU_NUM = @"^((13[0-2])|(145)|(15[5-6])|(176)|(18[5,6]))\\d{8}|(1709)\\d{7}$";
        /**
         * 电信号段正则表达式
         */
        NSString *CT_NUM = @"^((133)|(153)|(177)|(18[0,1,9]))\\d{8}$";
        NSPredicate *pred1 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM_NUM];
        BOOL isMatch1 = [pred1 evaluateWithObject:mobile];
        NSPredicate *pred2 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU_NUM];
        BOOL isMatch2 = [pred2 evaluateWithObject:mobile];
        NSPredicate *pred3 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT_NUM];
        BOOL isMatch3 = [pred3 evaluateWithObject:mobile];
        
        if (isMatch1 || isMatch2 || isMatch3) {
            return nil;
        }else{
            return @"请输入正确的电话号码";
        }
    }
    return nil;
}

+(void)showShareInController:(UIViewController *)controller andShareURL:(NSString *)urlStr andTitle:(NSString *)title andShareText:(NSString *) shareText andShareImage:(UIImage *)image
{
    [UMSocialQQHandler setQQWithAppId:@"1102294126" appKey:@"nFe21NjHs85gO6Ug" url:urlStr];
    [UMSocialWechatHandler setWXAppId:@"wxbb2714bf024ca19d" appSecret:@"d870a9e14d6df40bf5fbb2b64b4ee432" url:urlStr];
    [UMSocialData defaultData].extConfig.wxMessageType = UMSocialWXMessageTypeWeb;
    //分享图文样式到微信朋友圈显示字数比较少，只显示分享标题
    //[UMSocialData defaultData].extConfig.identifier = @"com.fenglun.CarService";
    [UMSocialData defaultData].extConfig.title = title;
    //设置微信好友或者朋友圈的分享url,下面是微信好友，微信朋友圈对应wechatTimelineData
    [UMSocialData defaultData].extConfig.wechatSessionData.url = urlStr;
    
    
    //分享内嵌文字
    //    @"IMG_0884"
    UIImage *shareImage = image;          //分享内嵌图片
    
    //如果得到分享完成回调，需要设置delegate为self
    NSArray *array = @[UMShareToSina,UMShareToTencent,UMShareToWechatSession,UMShareToWechatTimeline,UMShareToQzone,UMShareToQQ,UMShareToEmail,UMShareToSms];
    
    [UMSocialSnsService presentSnsIconSheetView:controller appKey:@"53d0785256240b7d54006566" shareText:shareText shareImage:shareImage shareToSnsNames:array delegate:nil];
}

#pragma mark ===跳转 条件判断==
//+(BOOL) isLoginAndIsNetValiadWitchController:(UIViewController *)controll{
//    
//    NSString *userid = KgetUserValueByParaName(USERID);
//    if (IsEmptyStr(userid)) {
//        LoginViewController *log = [[LoginViewController alloc]init];
//        YHNavgationController *logNav = [[YHNavgationController alloc]initWithRootViewController:log];
//        [controll presentViewController:logNav animated:YES completion:nil];
//        return NO;
//    }
//    
//    if (![YHJHelp isReachable]) {
//        
//        [WFHudView showMsg:@"请检查网络设置" inView:nil];
//        return NO;
//        
//    }
//    
//    return YES;
//}

#pragma mark- 跳转  条件判断自定义登录界面
+(BOOL) isLoginAndIsNetValiadWitchController:(UIViewController *)controll{
    

    NSString *userid = KgetUserValueByParaName(USERID);
    if (IsEmptyStr(userid)) {
        WWYLogInViewController *log = [[WWYLogInViewController alloc]init];
        YHNavgationController *logNav = [[YHNavgationController alloc]initWithRootViewController:log];
        [controll presentViewController:logNav animated:YES completion:nil];
        return NO;
    }
    
    if (![YHJHelp isReachable]) {
        
        [WFHudView showMsg:@"请检查网络设置" inView:nil];
        return NO;
        
    }
    return YES;
}







//判断是否有开启相机
+(BOOL)isCustom{
    BOOL Custom= [UIImagePickerController
                  isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];//判断摄像头是否能用
    
    if (Custom) {
        //  IOS 7 以上判断
        if(IOS7){
            AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
            if (authStatus == AVAuthorizationStatusRestricted || authStatus ==AVAuthorizationStatusDenied)
            { //无权限
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                                message:@"没有访问权限,请先在隐私-相机中设置"
                                                               delegate:nil
                                                      cancelButtonTitle:@"确定"
                                                      otherButtonTitles:nil];
                [alert show];
                
                return NO;
                
            }else{
                
                return YES;
            }
            
        }else{
            
            return YES;
            
        }
    }else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                        message:@"没有检测到摄像头"
                                                       delegate:nil
                                              cancelButtonTitle:@"确定"
                                              otherButtonTitles:nil];
        [alert show];
        return NO;
        
    }
    
}

#pragma mark- 修改视图圆角(自动布局结束在调用此方法)
+(void) setMaskTo:(UIView*)view byRoundingCorners:(UIRectCorner)corners WithRadii:(CGFloat)radian
{
    // bounds参数需要有值之后再传
    UIBezierPath* rounded = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:corners cornerRadii:CGSizeMake(radian, radian)];
    
    CAShapeLayer* shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];
    
    view.layer.mask = shape;
}

#pragma mark- 自适应label;
+(CGSize)labelRectWithSize:(CGSize)size LabelText:(NSString *)labelText Font:(UIFont *)font{
    
    NSDictionary  *dic = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    CGSize actualsize = [labelText boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    
    return actualsize;
    
}




+(void)showMessage:(NSString*)message
{
    if (IsEmptyStr(message)) {
        return;
    }
    
    if (message.length >=10) {
        alertContent(message);
    }else
    {
        [WFHudView showMsg:message inView:nil];
    }
}


@end
