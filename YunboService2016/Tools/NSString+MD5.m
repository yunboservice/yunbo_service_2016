//
//  NSString+MD5.m
//  TZS
//
//  Created by yandi on 14/12/9.
//  Copyright (c) 2014年 NongFuSpring. All rights reserved.
//

#import "NSString+MD5.h"
#import <CommonCrypto/CommonCrypto.h>
#import <CoreLocation/CoreLocation.h>
@implementation NSString (MD5)
+ (NSString *)md5:(NSString *)originalStr {
//    CC_MD5_CTX md5;
//    CC_MD5_Init (&md5);
//    CC_MD5_Update (&md5, [originalStr UTF8String], (CC_LONG)[originalStr length]);
//    
//    unsigned char digest[CC_MD5_DIGEST_LENGTH];
//    CC_MD5_Final (digest, &md5);
//    NSString *s = [NSString stringWithFormat: @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
//                   digest[0],  digest[1],
//                   digest[2],  digest[3],
//                   digest[4],  digest[5],
//                   digest[6],  digest[7],
//                   digest[8],  digest[9],
//                   digest[10], digest[11],
//                   digest[12], digest[13],
//                   digest[14], digest[15]];
//    return s;
    
    const char *cStr = [originalStr UTF8String];
    unsigned char result[16];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), result); // This is the md5 call
    
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
   
}



+(NSString*)CurrentTime1970{
    
    NSDate *date = [NSDate date];
    NSTimeInterval time  = date.timeIntervalSince1970;
    NSString *timeStr = [NSString stringWithFormat:@"%.f",time];
    return timeStr;
    
}

+(NSString *)userLocWith:(double) uerLog andLat:(double) userLat PublisLocWith:(double) Plog andLat:(double) Plat{
    
    CLLocation *userL = [[CLLocation alloc] initWithLatitude:userLat longitude:uerLog];
    CLLocation *PubL  = [[CLLocation alloc] initWithLatitude:Plat longitude:Plog];
    CLLocationDistance kilometers = [userL distanceFromLocation:PubL] / 1000;
   
    return [NSString stringWithFormat:@"%.2fkm",kilometers];
}

+(NSString*)TimeToCurrentTime:(NSNumber *)time{
    
    int giveTime = [time intValue];
    int returnTime ;
    int  mm      = 60;       //分
    int  hh      = mm * 60;  // 时
    int  dd      = hh * 24 ; // 天
    int  MM      = dd * 30;  // 月
    int  yy      = MM * 12;  // 年
    
    if (giveTime < mm) {
        return [NSString stringWithFormat:@"%d秒前",giveTime];//秒
    }else if(mm       <= giveTime && giveTime<hh){
        returnTime  = giveTime / mm ;
        return [NSString stringWithFormat:@"%d分钟前",returnTime];//分
    }else if (hh      <= giveTime && giveTime < dd){
        returnTime     =  giveTime /hh;
        return [NSString stringWithFormat:@"%d小时前",returnTime];
    }else if (dd      <= giveTime && giveTime < MM){
        returnTime     = giveTime / dd ;
        return [NSString stringWithFormat:@"%d天前",returnTime];
    }else if (MM      <= giveTime && giveTime < yy){
        returnTime     = giveTime / MM ;
        return [NSString stringWithFormat:@"%d月前",returnTime];
    }else if (yy <= giveTime){
        returnTime    = giveTime / yy ;
        return [NSString stringWithFormat:@"%d年前",returnTime];
    }
    return @"0秒前";
}


+(NSString*)CurrentTimeByStr
{
    NSDate *date = [NSDate date];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];//格式化
    
    [df setDateFormat:@"yyyy-MM-dd"];
    
    NSString * nowTimeStr = [df stringFromDate:date];
    
    return nowTimeStr;
}



@end
