//
//  CommenHead.h
//  yunbo2016
//
//  Created by apple on 15/10/9.
//  Copyright (c) 2015年 apple. All rights reserved.
//   通用配置

#import "UserDefautHead.h"
#import "NotifcationHead.h"
#import "UrlHead.h"
#import "EXTScope.h"
//data
//BOOL     is_ChuShiBaiDuNav = YES;




#ifndef YHJCommen_h
#define YHJCommen_h

//只有在少部分大量用到某个版本以上API的地方才使用
#define IOS9            ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 9.0)
#define IOS8            ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 8.0)
#define IOS7            ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 7.0)

//-------------------获取设备大小-------------------------
// NavBar高度
#define KNavigationBarHeight (44.0)
// 状态栏高度
#define KStatusBarHeight (20)
// 顶部高度
#define KTopHeight (KNavigationBarHeight + KStatusBarHeight)
// 底部 TabBar 高度
#define KTabBarHeight 49
// 动态获取屏幕宽高
#define KScreenHeight ([UIScreen mainScreen].bounds.size.height)
#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define SCREEN_MAX_LENGTH (MAX(KScreenWidth, KScreenHeight))
#define SCREEN_MIN_LENGTH (MIN(KScreenWidth, KScreenHeight))

#define IPHONE3_5INCH   ([[UIScreen mainScreen] bounds].size.height == 480)

#define IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

// appDelegate
#define YHJppDelegate ((AppDelegate *)[UIApplication sharedApplication].delegate)

// 随机数
#define KArcNum(x) arc4random_uniform(x)

// 颜色
#define KColorRGBA(r, g, b, a) [UIColor colorWithRed:(r) / 255.0 green:(g) / 255.0 blue:(b) / 255.0 alpha:(a)]
#define KColorRGB(r, g, b) KColorRGBA(r, g, b, 1.f)


// 十六进制转颜色
#define KColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

/*获得版本号*/
#define kVersion [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]


// Log
#ifdef DEBUG
#define MyLog(format, ...)                     do {                                                                          \
fprintf(stderr, "<%s : %d> %s\n",                                           \
[[[NSString stringWithUTF8String:__FILE__] lastPathComponent] UTF8String],  \
__LINE__, __func__);                                                        \
(NSLog)((format), ##__VA_ARGS__);                                           \
fprintf(stderr, "-------\n") ;                                          \
} while (0)
#else

#define MyLog(format, ...)

#endif

/**
 *  判断是否是空字符串 非空字符串 ＝ yes
 *
 *  @param string
 *
 *  @return 
 */

#define  NOEmptyStr(string)  string == nil ||[string isEqualToString: @""] || string == NULL ||[string isKindOfClass:[NSNull class]]||[[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0 ? NO : YES

/**
 *  判断是否是空字符串 空字符串 = yes
 *
 *  @param string
 *
 *  @return
 */
#define  IsEmptyStr(string) string == nil || string == NULL || [string isEqualToString:@""] ||[string isKindOfClass:[NSNull class]]||[[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0 ? YES : NO



#define KPostNotifiCation(postName,objectDic) [[NSNotificationCenter defaultCenter] postNotificationName:postName object:objectDic]

#define KAddobserverNotifiCation(ActionName,postName) [[NSNotificationCenter defaultCenter] addObserver:self selector:ActionName name:postName object:nil]



/**
 *  移除通知
 *
 *  @param ActionName 通知名称
 */
#define KRemoverNotifi(ActionName)    [[NSNotificationCenter defaultCenter] removeObserver:self name:ActionName object:nil]


/**
 *  添加本地偏好设置
 */

#define KsetUserValueByParaName(Object,Key) \
[[NSUserDefaults standardUserDefaults] setObject:Object forKey:Key];\
[[NSUserDefaults standardUserDefaults] synchronize];\

/**
 *  获取本地数据值
 */

#define KgetUserValueByParaName(Key)  [[NSUserDefaults standardUserDefaults] objectForKey:Key]



//------------------其他相关配置------------------
#define Font(FONT)  [UIFont systemFontOfSize:FONT]
#define KNavigationBarTitleFont 18
#define KNavigationBarItemTitleFont 15
#define kNavigationBarTintColor [UIColor whiteColor]
#define kNavigationBarColor     KColorFromRGB(0x3c3c3c) //文字深灰
#define kNavigationBarLineColor [UIColor colorWithWhite:0.869 alpha:1]
#define APPversion    [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString*)kCFBundleVersionKey]

#define MainTextColor           [UIColor blackColor]   // 主文字颜色
#define MainTextGrayColor       KColorFromRGB(0x5d5d5d)// 主文字中灰色颜色
#define MainTextLightGrayColor  KColorFromRGB(0x8c8c8c)//文字浅灰
#define MainTextOrangeColor     KColorFromRGB(0xFB6300)// 主文字橘色颜色
#define MainTextGreenColor  KColorFromRGB(0x2B9C24) //主文字颜色 绿色

#define MainBackColor           KColorFromRGB(0xefeef3)// 主背景色
#define MainBtnColor            KColorFromRGB(0x5bb431)//主按钮绿色背景
#define MainBtnUColor           KColorFromRGB(0x969696)//主按钮灰色背景

//*******车友圈相关配置**********//

#define ShowImage_H    70 //车友圈图片高度
#define kDistance  10 //说说和图片的间隔
#define limitline       4  //显示最大行数
#define offSet_X       48  //车友圈文字左边起始距离
#define kSelf_SelectedColor [UIColor colorWithWhite:0 alpha:0.4] //点击背景  颜色
#define kUserName_SelectedColor [UIColor colorWithWhite:0 alpha:0.25]//点击姓名颜色

#define DELAYEXECUTE(delayTime,func) (dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{func;}))

/*提示弹窗的初始化 走代理*/
#define alertContent(content) \
UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"云泊" \
message:content \
delegate:self   \
cancelButtonTitle:@"确定" \
otherButtonTitles:nil];  \
[alert show];

/*提示弹窗的初始化 不走代理*/
#define alertUnContent(content) \
UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"云泊" \
message:content \
delegate:nil   \
cancelButtonTitle:@"确定" \
otherButtonTitles:nil];  \
[alert show];

#endif
