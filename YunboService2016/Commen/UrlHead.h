//
//  UrlHead.h
//  yunbo2016
//
//  Created by apple on 15/10/22.
//  Copyright © 2015年 apple. All rights reserved.
//

#ifndef UrlHead_h
#define UrlHead_h

#ifdef DEBUG
#define MainAPI  @"http://dev.api.ybzg.com/v4_1/"//@"http://dev.api.ybzg.com/v4_1/"
#else
#define MainAPI  @"http://prod.api.ybzg.com/v4_1/"
#endif

#define YUNBOXIEYI     @"http://aweb.ybzg.com/usage/agreement"    //服务协议

#define DIDIWEBURL  @"http://page.kuaidadi.com/m/dj.html?sc=11&daijia_lat=123&daijia_lng=45" //滴滴代驾

#define HCFINDERNOTI  [NSString stringWithFormat:@"http://api.ybzg.com/CloudParking/web/notice/index.php?userID=%@",KgetUserValueByParaName(USERID)]  //发现页面_通知url

#define HBLQline @"http://www.ybzg.com/weixin/red_envelopes.php?userId=" // 红包领取
#define HBShare  @"http://www.ybzg.com/weixin/red_share.php?userId=" // 红包分享
#define YUNBIWEB [NSString stringWithFormat:@"http://api.ybzg.com/CloudParking/web/price/yunbi.php?userid=%@",KgetUserValueByParaName(USERID)]  //云币玩法
#define FW_ServiceDescription @"http://aweb.ybzg.com/hb/parking" //服务说明
//协议
#define YUNBO_PROTOCAL @"http://aweb.ybzg.com/usage/agreement"
// itunes
#define YUNBO_ITUNES @"https://itunes.apple.com/cn/app/yun-po/id908881538?l=zh&ls=1&mt=8"
//云泊下载
#define YUNBO_DOWNLOADS @"http://download.ybzg.com/app"
#endif /* UrlHead_h */
