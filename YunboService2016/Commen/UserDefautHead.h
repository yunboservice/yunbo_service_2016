//
//  UserDefautHead.h
//  yunbo2016
//
//  Created by apple on 15/10/9.
//  Copyright (c) 2015年 apple. All rights reserved.
// 本地偏好设置

#import <Foundation/Foundation.h>


extern NSInteger isPush ;



#ifndef  LocUserDefautConfig_h
#define  LocUserDefautConfig_h

typedef enum{
    
    YMTableViewCellType_Bangbang    = 2,
    YMTableViewCelltype_Saisai      = 1,
    YMTableViewCelltype_Activit     = 3,
    
} YMTableViewCelltype;


typedef NS_ENUM(NSInteger, GestureType) {
    
    TapGesType = 1,
    LongGesType,
    
};


UIKIT_EXTERN NSString * const REQUEST_CODE;
UIKIT_EXTERN NSString * const REQUEST_MESSAGE;



#define USERSTATE             @"userState" //用户状态
#define USERID                @"userID"  //用户id
#define HCmobile              @"mobile"          //手机号码
#define HCphotourl            @"photourl"        //头像图片地址
#define HCuserName            @"userName"        //用户昵称
#define HCsex                 @"sex"             //性别
#define HCplateType           @"NewplateType"       //车牌存放的数组 （NSArray）
#define HCclearAddr           @"cleaningAddr"    //历史洗车地址
#define HCparkAddr            @"parkingAddr"     //历史泊车地址
#define HCfixedCarport        @"fixedCarport"    //固定车位数
#define HCuserSecret          @"secret"          //用户登录保存的秘匙
#define PushToken             @"pushToken"       //推送码
#define HCPARKINGTS        @"parkingTS"             //泊车预停时间
#define HCCleaningTS        @"cleaningTS"             //洗车时间
#define HCDiscoveryDyItems        @"discoveryDyItems"   //发现页面 Web 数据
#define HCParkingApRange        @"parkingApRange"   //泊车 预预订的范围 例3000

#define HCCLEANINGCANCELREASON        @"cleaningCancelReason"             //洗车取消理由选项
#define HOMEBGURL             @"indexBgUrl"      //首页背景图
#define HCParkingCouponUrl      @"parkingCouponUrl"  //泊车优惠券链接
#define HCParkingCouponShareUrl @"parkingCouponShareUrl"//泊车优惠券分享链接
#define HCkfPhone             @"CSPhones"  //客服电话
#define HCisReleased          @"isReleased" //app是否是发布
#define HCparkingTimeout      @"parkingTimeout" //停车超时
#define HCBalance             @"Balance"         // 现金余额
#define HCcpAmount            @"cpAmount"        //车位数量
#define HCyunbiBalance        @"yunbiBalance"    // 云币余额
#define HChbBalance           @"hbBalance"       //红包金额

#define HCuserLoctionLat      @"userLoctionLat"  //用户经度
#define HCuserLoctionLng      @"userLoctionLng"  //用户纬度

#define HCCountDownTime       @"countDownTime" //开始时间
//车辆
#define HCAllWaysCar          @"allwaysCar"      // 默认常用车牌
#define HCOUTRegistTime         @"outRegistTime"   // 记录退出注册页面的时间


/** 朋友圈  */
#define TABLE_FRIENDROUND @"FriendRoundList" //朋友圈列表
#define TABLE_FRIENDROUNDIMAGE @"FriendRoundImage" // 朋友圈图片缓存
#define TABLE_FRIENDROUNDCOMMENTLIST @"FriendRoundcommentList"//

#define SHARE_MESSAGE  @"share_message"  //邀请好友所发送的内容
#define FIRSTPUBLISH   @"firstpublish"   //第一次发布的时的提示页面

#define TABLE_FRIENDS @"FriendList"//好友列表
#define TABLE_ADDRESSBOOKS @"AddressBooks"//通讯录列表
#define TABLE_PUSHS       @"pushs"   //推送信息表
#define TABLE_ASSOCIATOR @"associator" //会员表
#define YONGLIAN_PUSH_ACOUNT_BANGBNAG  @"87830600000110"//容联用于推送-帮帮
#define YONGLIAN_PUSH_ACOUNT_FRIEND  @"87830600000111"//容联用于推送-好友
#define YONGLIAN_PUSH_ACOUNT_MOVECAR @"87830600000112"//容联用于推送-车移移
#define YONGLIAN_PUSH_ACOUNT_WASHORDER  @"87830600000113"//容联用于推送-洗车订单
#define YONGLIAN_PUSH_ACOUNT_PARKORDER  @"87830600000114"//容联用于推送-泊车订单
#define YONGLIAN_PUSH_ACOUNT_NEWS  @"87830600000115"//容联用于推送-通知
#define YONGLIAN_PUSH_ACOUNT_COUPON  @"87830600000116"//容联用于推送-优惠券
#define YONGLIAN_PUSH_ACOUNT_HONGBAO  @"87830600000115"//容联用于推送-红包


#define DIDREFRESHUSERBASEINFO @"refreshUserBaseInfo" //刷新好友详细页面
#define DIDRECEIVEFRIEND  @"didReceiveFriend"  //刷新好友列表
#define DIDRECEIVEORDERCARMSG @"didReceiveOrder_carmsg"
#define DIDRECEIVEORDERSTATECHANGE  @"didReceiveOrderStateChange"  //订单状态
#define DIDRECEIVEPARKCARPORTNAVIGATION @"didReceiveParkCarportNavigation" //车位导航 推送
#define DIDRECEIVEORDERUSERENTER @"didReceiveOrder_userEnter"
#define DIDRECEIVEORDERUSERLEAVE @"didReceiveOrder_userLeave"
#define DIDRECEIVECHATSENDMSG @"didReceiveChat_sendMsg"    //
#define DIDRECEIVECHATRECEIVEMSG @"didReceiveChat_receivedMsg"  //
#define DIDRECEIVEORDERSENT @"didReceiveOrder_sent"     //
#define DIDRECEIVEORDERTAKEN @"didReceiveOrder_taken"  //
#define DIDRECEIVEORDERBACK @"didReceiveOrder_back"
#define DIDRECEIVEMSG @"didReceiveMsg"
#define DIDREDPOINTBANGBANG @"didRedPointBangBang"//帮帮的红点设置
/** 洗车订单状体5  */

#define KWO_WAIT_USER_PAY @"WAIT_USER_PAY"//等待支付
#define KWO_SERVICE_NOREPLY @"SERVICE_NOREPLY"//等待接单
#define KWO_SERVICE_TAKEN @"SERVICE_TAKEN"//接单
#define KWO_SERVICE_ING @"SERVICE_ING"//开始服务
#define KWO_SERVICE_FINISHED @"SERVICE_FINISHED"//结束服务
#define KWO_SERVICE_CANCEL @"SERVICE_CANCEL"//取消订单
//泊车

#define KWO_SERVICE_TIMEOUT @"SERVICE_TIMEOUT"//


#define KWO_SERVICE_OK @"SERVICE_OK"//

#define KWO_PRE_ORDER @"PRE_ORDER"//


/**  扫码 部分**/

#define HCScanCodeStr_User    @"ybzg2015^*%H23i734Q$@"     // 扫描的可识别字符串  用户生成
#define HCScanCOdeStr_Service @"ybzg2014^*%H23i734Q$@"  // 扫描的可识别字符串  服务者生成
#define HCScanCodeStr_Chongzhi @"ybzgChongZhi^*%H23i734Q$@" //扫描的可识别字符串  服务生成 充值码
#define HCScanCodeStr_YouHui  @"ybzgYouhui^*%H23i734Q$@" //扫描的可识别字符串 服务者生成 优惠券
#define HCScanCodeStr_AddFriend @"ybzgFriend^*%H23i734Q$@" //扫描加好友
#define HCScanPostName          @"scanPostName"           //其他页面的扫码回调



#endif