//
//  NotifcationHead.h
//  yunbo2016
//
//  Created by apple on 15/10/9.
//  Copyright (c) 2015年 apple. All rights reserved.
//  通知设置

#ifndef NotifcationHeadConfig_h
#define NotifcationHeadConfig_h


#define WEICHATPAYRESULT @"WEICHATPAYRESULT" //微信支付


#define FriendReply            @"friendreply"    // 朋友圈的回复
#define FriendFinishHelp       @"friendFinishHelp" //完成帮助
#define ShowCouponRefresh      @"showCouponrefresh" // success got coupon and refresh tableView
#define FriendMainTableRefresh @"FriendMainTableRefresh"
#define HCScanPostName          @"scanPostName"           //其他页面的扫码回调




#endif