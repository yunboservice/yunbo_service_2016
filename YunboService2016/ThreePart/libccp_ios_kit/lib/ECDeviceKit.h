//
//  ccp_ios_kit.h
//  ccp_ios_kit
//
//  Created by wangming on 15/7/8.
//  Copyright (c) 2015年 ronglian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ECKitDeviceDelegate.h"
#import "ECIMKitManager.h"
#import "ECPersonInfo.h"
#import "ECLoginInfo.h"

@interface ECDeviceKit : NSObject<UIApplicationDelegate>
{

}

/**
 @brief device代理
 @discussion 用于监听通知事件
 */
@property (nonatomic, copy) NSString* userId;

/**
 @brief device代理
 @discussion 用于监听通知事件
 */
@property (nonatomic, assign) id<ECKitDeviceDelegate> delegate;

/**
 @brief 即时消息界面管理类
 @discussion 用于IM的界面库管理
 */
@property (nonatomic, strong) ECIMKitManager* imKitManager;
/**
 @brief 被叫弹出页面的容器
 @discussion 被叫弹出页面的容器，不设置则不会弹出被叫音视频页面
 */
@property (nonatomic, strong) UIViewController* containerViewController;

/**
 @brief 单例
 @discussion 获取该类单例进行操作
 @return 返回类实例
 */
+(ECDeviceKit*)sharedInstance;

/**
 @brief
 @discussion 初始化SDK，根据传入的userId建立本地库
 @return 返回类实例
 */
+(ECDeviceKit*)initWithUserId:(NSString *)userId;

/**
 @brief 登录
 @discussion 异步函数，建立与平台的连接
 @param info 登录所需信息
 @param completion 执行结果回调block
 */
-(void)login:(ECLoginInfo *)info completion:(void(^)(ECError* error)) completion;

/**
 @brief 退出登录
 @discussion 异步函数，断开与平台的连接;该函数调用后SDK不再主动重连服务器
 @param completion 执行结果回调block
 */
-(void)logout:(void(^)(ECError* error)) completion;

/**
 @brief 切换服务器环境
 @discussion 调用登录接口前，调用该接口切换服务器环境；不调用该函数，默认使用的是生产环境；
 @param isSandBox 是否沙河环境
 @return 是否成功 0:成功 非0失败
 */
-(NSInteger)SwitchServerEvn:(BOOL)isSandBox;

/**
 @brief 设置个人信息
 @param person 个人信息
 @param completion 执行结果回调block
 */
-(void)setPersonInfo:(ECPersonInfo*)person completion:(void(^)(ECError* error, ECPersonInfo* person)) completion;

/**
 @brief 获取个人信息
 @param completion 执行结果回调block
 */
-(void)getPersonInfo:(void(^)(ECError* error, ECPersonInfo* person)) completion;

/**
 @brief 启动会话列表界面
 @discussion 调用后启动会话列表界面
 @return 无返回值
 */
-(void) startConversationListWithNav:(UINavigationController *)nav;

/**
 @brief 设置会话列表界面标题
 @discussion 根据提供的参数设置会话列表界面
 @param title 标题的内容
 @return 无返回值
 */
-(void) setConversationListNavigationTitle:(NSString*)title;

/**
 @brief 设置会话列表界面的返回界面图标
 @discussion 根据提供的UIImage设置会话列表界面的返回界面图标
 @param img 图片
 @return 无返回值
 */
-(void) setConversationListNavigationBackImg:(UIImage*) img;

/**
 @brief 设置会话列表界面的返回界面导航栏右侧按钮
 @discussion 根据提供的UIView设置会话列表界面的返回界面导航栏右侧按钮
 @param view 设置到右侧按钮的view
 @return 无返回值
 */
-(void) setConversationListNavigationRightView:(UIView*) view;

/**
 @brief 设置无会话内容时候的界面
 @discussion 没有会话内容时，可以设置界面
 @param view 无会话内容时显示的view
 @return 无返回值
 */
-(void) setNoConversationView:(UIView*) view;

/**
 @brief 启动会话界面
 @discussion 调用后启动会话界面
 @param conversationType 会话类型 0是普通会话，1是通知消息
 @param sId 会话ID（对方名称或者群id）
 @param title 标题
 @return 无返回值
 */
-(void) startConversation:(int) conversationType andSid:(NSString*) sId andTitle:(NSString*) title andNav:(UINavigationController *)navigationController;

/**
 @brief 设置会话界面标题
 @discussion 根据提供的参数设置会话界面
 @param title 标题的内容
 @param ori   标题的位置，左中右
 @return 无返回值
 */
-(void) setConversationNavigationTitle:(NSString*) title;

/**
 @brief 设置会话界面的返回图标
 @discussion 根据提供的UIImage设置会话界面的返回图标
 @param drawable 图片
 @return 无返回值
 */
-(void) setConversationNavigationBackImg:(UIImage*) img;

/**
 @brief 设置会话界面的返回界面导航栏右侧按钮
 @discussion 根据提供的UIView设置会话界面的返回界面导航栏右侧按钮
 @param view 设置到右侧按钮的view
 @return 无返回值
 */
-(void) setConversationNavigationRightView:(UIView*) view;

/**
 @brief 启动群组列表界面
 @discussion 调用后启动群组会话列表界面
 @param nav 导航栏
 @return 无返回值
 */
-(void) startGroupListWithNav:(UINavigationController *)nav;

/**
 @brief 设置会话列表界面标题
 @discussion 根据提供的参数设置会话列表界面
 @param title 标题的内容
 @return 无返回值
 */
-(void) setGroupListNavigationTitle:(NSString*) title;

/**
 @brief 设置群组列表界面的返回界面图标
 @discussion 根据提供的UIImage设置会话列表界面的返回界面图标
 @param drawable 图片
 @return 无返回值
 */
-(void) setGroupListNavigationBackImg:(UIImage*) img;


/**
 @brief 设置群组列表界面的返回界面导航栏右侧按钮
 @discussion 根据提供的UIView设置会话列表界面的返回界面导航栏右侧按钮
 @param view 设置到右侧按钮的view
 @return 无返回值
 */
-(void) setGroupListNavigationRightView:(UIView*) view;

/**
 @brief 启动语音通话界面
 @discussion 根据传入的参数启动语音通话界面
 @param vc 语音通话界面将会在vc下presentViewController
 @param caller 被叫voip号码
 @param callerNickname 被叫昵称
 @return 无返回值
 */
-(void)startCallViewWithViewController:(UIViewController *)vc andCallerNumber:(NSString*) caller andCallerNickname:(NSString*)callerNickname;

/**
 @brief 启动视频通话界面
 @discussion 根据传入的参数启动语音通话界面
 @param vc 语音通话界面将会在vc下presentViewController
 @param caller 被叫voip号码]
 @param callerNickname 被叫昵称
 @return 无返回值
 */
-(void)startVideoViewWithViewController:(UIViewController *)vc andCallerNumber:(NSString*) caller andCallerNickname:(NSString*)callerNickname;
@end
