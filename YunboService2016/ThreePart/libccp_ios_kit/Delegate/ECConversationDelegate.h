//
//  ECConversationDelegate
//  CCPiPhoneSDK
//
//  Created by jiazy on 15/5/18.
//  Copyright (c) 2015年 ronglian. All rights reserved.
//


#import "ECDeviceKitDelegateBase.h"
//会话界面回调
@protocol ECConversationDelegate <ECDeviceKitDelegateBase>
@optional

/**
 @brief 点击聊天右侧导航触发
 @discussion 进入好友信息界面 或者 群组管理界面
 @param tyle  1 好友信息   2  群组信息
 @param sessionId 群组ID  或者  好友ID
 @return
 */
- (void)onRightavigationBarClick:(NSInteger)type andSId:(NSString *)sid;

/**
 @brief 自定义长按消息子条目触发
 @discussion 没有实现该方法，进行默认的操作
 @param message 消息
 @return
 */
- (void)onCustomChatListItemLongClick:(ECMessage *)message;

/**
 @brief 自定义点击人物头像触发
 @discussion 没有实现该方法，进行默认的操作
 @param message 消息
 @param isSender 是否是发送者
 @return
 */
- (void)onMessagePortRaitClick:(ECMessage *)message IsSender:(BOOL)isSender;

/**
 @brief 返回自定义扩展标题
 @discussion 没有实现该方法，进行默认的操作
 @param
 @return 自定义扩展标题数组
 */
- (NSArray *)getCustomPlusTitleArray;

/**
 @brief 返回自定义扩展图片
 @discussion 没有实现该方法，进行默认的操作
 @param
 @return 自定义扩展图片数组
 */
- (NSArray *)getCustomPlusDrawableArray;

/**
 @brief 自定义点击Item 事件触发
 @discussion 根据title相应的处理item点击事件 否则进行默认操作
 @param title 自定义扩展标题
 @return
 */
- (void)onPlusExtendedItemClick:(NSString *)title;

/**
 @brief 返回自定义群组成员列表
 @discussion 没有实现该方法，进行默认的操作
 @param
 @return 自定义扩展成员数组
 */
- (NSArray *)getGroupMemberListArray;


@required
@end