//
//  ECKitDelegateBase
//  CCPiPhoneSDK
//
//  Created by jiazy on 14/11/13.
//  Copyright (c) 2014年 ronglian. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "ECError.h"
#import "ECEnumDefs.h"
#import "ECSession.h"
#import "ECMessage.h"

/**
 * 该代理用于接收登录和注销状态
 */
@protocol ECDeviceKitDelegateBase <NSObject>

@optional


@required

@end
