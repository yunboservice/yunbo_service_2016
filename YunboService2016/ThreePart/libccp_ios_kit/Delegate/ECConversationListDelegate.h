//
//  ECConversationListDelegate
//  CCPiPhoneSDK
//
//  Created by jiazy on 14/11/16.
//  Copyright (c) 2014年 ronglian. All rights reserved.
//

#import "ECDeviceKitDelegateBase.h"
#import "ECEnumDefs.h"

/**
 * 该代理用于处理会话列表的回调
 */
@protocol ECConversationListDelegate <ECDeviceKitDelegateBase>

@optional
/**
 @brief 设置无会话内容时候的界面
 @discussion 没有会话内容时，可以设置界面
 @return 返回view 无会话内容时显示的view
 */
- (UIView *)setNoConversationView;

/**
 @brief 点击会话子条目进行的操作
 @discussion 没有实现该方法，默认进入会话界面
 @param session 会话条目
 @return
 */
- (void)onCustomConversationItemClick:(ECSession *)session;

/**
 @brief 长按会话子条目进行的操作
 @discussion 没有实现该方法，进行默认的操作，删除
 @param session 会话条目
 @return
 */
- (void)onCustonConversationLongClick:(ECSession *)session;

/**
 @brief 会话列表右侧导航时点击触发
 @discussion
 @return
 */
- (void)onCustomConversationListRightavigationBarClick;




@required
@end