//
//  ECPersonInfo.h
//  CCPiPhoneSDK
//
//  Created by jiazy on 15/3/24.
//  Copyright (c) 2015年 ronglian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ECEnumDefs.h"

@interface ECPersonInfo : NSObject
//昵称
@property (nonatomic, copy) NSString *nickName;
//性别
@property (nonatomic, assign) ECSexType sex;
//生日 时间格式 yyyy-MM-dd
@property (nonatomic, copy) NSString *birth;
//信息版本
@property (nonatomic, assign) long long version;
@end
