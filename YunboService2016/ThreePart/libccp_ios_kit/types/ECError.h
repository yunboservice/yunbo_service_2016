//
//  ECError.h
//  CCPiPhoneSDK
//
//  Created by jiazy on 14/11/5.
//  Copyright (c) 2014年 ronglian. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * 错误类型枚举值
 */
typedef NS_ENUM(NSInteger,ECErrorType) {
    
    /** 服务器连接中... */
    ECErrorType_Connecting = 100,
    
    /** 无错误，成功 */
    ECErrorType_NoError = 200,
    
    /** 内容长度超过规定 */
    ECErrorType_ContentTooLong = 170001,
    
    /** 必选参数为空 */
    ECErrorType_IsEmpty = 170002,
    
    /** 未登录 */
    ECErrorType_NotLogin = 170003,
    
    /** 发送消息失败 */
    ECErrorType_SendMsgFailed = 170004,
    
    /** 正在录音，当前只能录制一个，请先停止 */
    ECErrorType_IsRecording = 170005,
    
    /** 录音超时，60秒长度 */
    ECErrorType_RecordTimeOut = 170006,
    
    /** 录音已经停止，当前未录音调用停止录音 */
    ECErrorType_RecordStoped = 170007,
    
    /** 录音时间过短，不超过1秒 */
    ECErrorType_RecordTimeTooShort = 170008,
    
    /** 文件不存在 */
    ECErrorType_FileNotExist = 170011,
    
    /** 传入的参数类型错误 */
    ECErrorType_TypeIsWrong = 170012,
    
    /** 您的设备在其他地方登陆，被踢 */
    ECErrorType_KickedOff = 175004,
    
    /** token认证失败 */
    ECErrorType_TokenAuthFailed = 520016,
    
    /** 鉴权服务器异常 */
    ECErrorType_AuthServerException = 529999,
    
    /** 连接服务器异常 */
    ECErrorType_ConnectorServerException = 559999,
    
    /** 已经加入在群组中 */
    ECErrorType_Have_Joined = 590017,
    
    /** 发送文本被禁言 */
    ECErrorType_Have_Forbid = 580010,
    
    /** 不在该群组中 */
    ECErrorType_File_NoJoined =560072,
    
    /** 发送附件被禁言 */
    ECErrorType_File_Have_Forbid = 560073,
};

/**
 * 错误类
 */
@interface ECError : NSObject

/**
 @property
 @brief 错误类型
 */
@property (nonatomic, readonly) ECErrorType errorCode;

/**
 @property
 @brief 错误类型描述
 */
@property (nonatomic, copy) NSString *errorDescription;

/**
 @param errorCode 错误类型
 @return 错误
 */

+ (ECError *)errorWithCode:(ECErrorType)errorCode;

/**
 @param error 错误
 @return 错误
 */
+ (ECError *)errorWithNSError:(NSError *)error;

@end
