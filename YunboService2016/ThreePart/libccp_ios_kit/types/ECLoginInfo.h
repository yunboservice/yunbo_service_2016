//
//  ECInitialization.h
//  CCPiPhoneSDK
//
//  Created by jiazy on 14/11/6.
//  Copyright (c) 2014年 ronglian. All rights reserved.
//

#import <Foundation/Foundation.h>


/**
 * 登录模式
 */
typedef NS_ENUM(NSUInteger,LoginMode)
{
    /** 用户输入密码登录模式，可以把其他设备踢出 默认值*/
    LoginMode_InputPassword = 1,
    
    /** 直接读取配置登录，如果在其他设备登录过，验证被踢出 */
    LoginMode_AutoInputLogin = 2
};

/**
 * 登录认证类型
 */
typedef NS_ENUM(NSUInteger,LoginAuthType)
{
    /** 正常认证模式，服务器认证appKey、appToken、username字段 默认值*/
    LoginAuthType_NormalAuth = 1,
    
    /** 密码认证模式，服务器认证appKey、username、userPassword字段 */
    LoginAuthType_PasswordAuth = 3
};

/**
 * 登录所需信息
 */
@interface ECLoginInfo : NSObject

/**
 @brief mode 认证类型
 */
@property (nonatomic, assign) LoginAuthType authType;

/**
 @brief mode 登录模式
 */
@property (nonatomic, assign) LoginMode mode;

/**
 @brief username 登录用户名，需要第三方用户自己维护
*/
@property (nonatomic, copy) NSString *username;

/**
 @brief userPassword 用户密码字段
 */
@property (nonatomic, copy) NSString *userPassword;

/**
 @brief appKey 用户在云通讯平台生成应用时的appKey
 */
@property (nonatomic, copy) NSString *appKey;

/**
 @brief appToken 用户在云通讯平台生成应用时的appToken
 */
@property (nonatomic, copy) NSString *appToken;

@end
