//
//  ECSpeakStatus.h
//  CCPiPhoneSDK
//
//  Created by jiazy on 14/11/7.
//  Copyright (c) 2014年 ronglian. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * 重连状态值
 */
typedef NS_ENUM(NSUInteger,ECConnectState) {
    
    /** 连接成功 */
    State_ConnectSuccess=0,
    
    /** 连接中 */
    State_Connecting,
    
    /** 连接失败 */
    State_ConnectFailed
};

/**
 * 网络状态值
 */
typedef NS_ENUM(NSInteger,ECNetworkType) {
    
    /** 当前无网络 */
    ECNetworkType_NONE=0,
    
    /** 当前网络是WIFI */
    ECNetworkType_WIFI,
    
    /**  当前网络是4G*/
    ECNetworkType_4G,
    
    /** 当前网络是3G */
    ECNetworkType_3G,
    
    /**  当前网络是GPRS*/
    ECNetworkType_GPRS,
    
    /** 当前网络为LAN类型 */
    ECNetworkType_LAN,
};

/**
 * 群组成员禁言状态
 */
typedef NS_ENUM(NSInteger,ECSpeakStatus){
    
    /** 允许发言 */
    ECSpeakStatus_Allow=1,
    
    /** 禁止发言 */
    ECSpeakStatus_Forbid
};

/**
 * 性别
 */
typedef NS_ENUM(NSInteger,ECSexType){
    
    /** 男 */
    ECSexType_Male=1,
    
    /** 女 */
    ECSexType_Female
};

/**
 * 群组成员角色
 */
typedef NS_ENUM(NSInteger,ECMemberRole){
    
    /** 创建者 */
    ECMemberRole_Creator=1,
    
    /** 管理员 */
    ECMemberRole_Admin,
    
    /** 普通成员 */
    ECMemberRole_Member
};

/**
 * 验证类型
 */
typedef NS_ENUM(NSInteger,ECAckType) {
    
    /** 不需要验证 */
    EAckType_None=0,
    
    /** 拒绝 */
    EAckType_Reject=1,
    
    /** 同意 */
    EAckType_Agree=2,
    
    /** 已处理 */
    EAckType_Done=3,
};

/**
 * 发送状态
 */
typedef NS_ENUM(NSInteger,ECMessageState) {
    
    /** 发送失败 */
    ECMessageState_SendFail=-1,
    
    /** 发送成功 */
    ECMessageState_SendSuccess=0,
    
    /** 发送中 */
    ECMessageState_Sending=1,
    
    /** 接收成功 */
    ECMessageState_Receive=2,
};

