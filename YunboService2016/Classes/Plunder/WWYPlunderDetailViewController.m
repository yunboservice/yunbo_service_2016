//
//  WWYPlunderDetailViewController.m
//  YunboService2016
//
//  Created by 王维一 on 16/3/17.
//  Copyright © 2016年 CarService. All rights reserved.
//

#import "WWYPlunderDetailViewController.h"

@interface WWYPlunderDetailViewController ()

@property (nonatomic, strong) UIView *back;

@property (nonatomic, strong) UIImageView *carIcon;

@property (nonatomic, strong) UILabel *carId;

@property (nonatomic, strong) UIImageView *positionIcon;

@property (nonatomic, strong) UILabel *address;

@property (nonatomic, strong) UILabel *more;

@property (nonatomic, strong) UILabel *moreService;

@property (nonatomic, strong) UILabel *mark;

@property (nonatomic, strong) UILabel *note;

@property (nonatomic, strong) UIButton *plunderBtn;

@property (nonatomic, strong) BMKMapView *mapView;


@end

@implementation WWYPlunderDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor grayColor];
    
    [self setupLayout];
    [self naviConfig];
}

- (void)naviConfig{
    self.sc_navigationItem.title = @"抢单详情";
    self.sc_navigationBar.backgroundColor = [UIColor whiteColor];
    
    SCBarButtonItem *btn = self.sc_navigationItem.leftBarButtonItem;
    [btn.button mas_updateConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(35, 35));
        
    }];

    
}
- (void)setupLayout{
    
    // 车辆小图标
    _carIcon = [UIImageView new];
    _carIcon.image = [UIImage imageNamed:@"ico_car"];
    [self.view addSubview:self.carIcon];

    [_carIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(KTopHeight).offset(KTopHeight + 10);
        make.left.mas_equalTo(15);
        make.size.mas_equalTo(CGSizeMake(30, 30));
    }];
    
    // 车牌号
    _carId = [UILabel new];
    _carId.text = @"车牌号";
    [self.view addSubview:_carId];
    [_carId mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_carIcon);
        make.left.mas_equalTo(_carIcon.mas_right).offset(10);
        make.height.mas_equalTo(_carIcon);
        make.right.mas_equalTo(0);
    }];
    
    // 地址小图标
    _positionIcon = [UIImageView new];
    _positionIcon.image = [UIImage imageNamed:@"ico_addr"];
    [self.view addSubview:_positionIcon];
    [_positionIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_carIcon.mas_bottom).offset(5);
        make.left.mas_equalTo(_carIcon);
        make.size.mas_equalTo(_carIcon);
    }];
    
    // 地址
    _address = [UILabel new];
    _address.text = @"地址";
    [self.view addSubview:_address];
    [_address mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_positionIcon);
        make.left.equalTo(_positionIcon.mas_right).offset(10);
        make.size.mas_equalTo(_carId);
    }];
    
    // 增值
    _more = [UILabel new];
    _more.text = @"增值服务:";
    _more.backgroundColor = [UIColor greenColor];
    [self.view addSubview:_more];
    [_more mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_positionIcon.mas_bottom);
        make.left.mas_equalTo(_positionIcon);
        make.size.mas_equalTo(CGSizeMake(80, 30));
    }];
    
    // 增值服务
    _moreService = [UILabel new];
    _moreService.text = @"无";
    [self.view addSubview:_moreService];
    [_moreService mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_more);
        make.left.mas_equalTo(_more.mas_right);
        make.height.mas_equalTo(_more);
        make.right.mas_equalTo(0);
    }];
    
    // 备注
    _mark = [UILabel new];
    _mark.text = @"备注:";
    _mark.backgroundColor = [UIColor blueColor];
    [self.view addSubview:_mark];
    [_mark mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_more.mas_bottom);
        make.left.mas_equalTo(_more);
        make.size.mas_equalTo(CGSizeMake(50, 30));
    }];
    
    // 备注详情
    _note = [UILabel new];
    _note.text = @"无";
    [self.view addSubview:_note];
    [_note mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_mark);
        make.left.mas_equalTo(_mark.mas_right);
        make.size.mas_equalTo(_moreService);
    }];
    
    // 抢单按钮
    _plunderBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    _plunderBtn.backgroundColor = MainBtnColor;
    [_plunderBtn setTitle:@"抢单" forState:UIControlStateNormal];
    [self.view addSubview:_plunderBtn];
    [_plunderBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_note.mas_bottom).offset(10);
        make.centerX.mas_equalTo(self.view.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(200, 40));
    }];
    
    // 地图
    _mapView = [BMKMapView new];
    [self.view addSubview:_mapView];
    [_mapView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_plunderBtn.mas_bottom).offset(10);
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];
    
    
    
    
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
