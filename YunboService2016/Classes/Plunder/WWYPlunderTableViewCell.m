//
//  WWYPlunderTableViewCell.m
//  YunboService2016
//
//  Created by 王维一 on 16/3/17.
//  Copyright © 2016年 CarService. All rights reserved.
//

#import "WWYPlunderTableViewCell.h"

@interface WWYPlunderTableViewCell ()

@property (nonatomic, strong) UIView *back;

@property (nonatomic, strong) UILabel *carId;

@property (nonatomic, strong) UIImageView *positionIcon;

@property (nonatomic, strong) UILabel *address;

@property (nonatomic, strong) UILabel *moreServer;

@property (nonatomic, strong) UILabel *time;

@property (nonatomic, strong) UIButton *plunder;


@end

@implementation WWYPlunderTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupLayout];
    }
    return self;
}

- (void)setupLayout{
    // cell的背景
    _back = [[UIView alloc] initWithFrame:CGRectMake(5, 0, [UIScreen mainScreen].bounds.size.width - 10, 140)];
    _back.backgroundColor = [UIColor grayColor];
    [YHJHelp setMaskTo:_back byRoundingCorners:UIRectCornerAllCorners WithRadii:5.0f];
    [self.contentView addSubview:_back];
    
    // 车牌号 (下面控件都以carId拘束)
    _carId = [UILabel new];
    _carId.text = @"车牌号";
    [_back addSubview:_carId];
    [_carId mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(10);
        make.size.mas_equalTo(CGSizeMake(200, 25));
    }];
    
    // 地址图标
    _positionIcon = [UIImageView new];
    _positionIcon.image = [UIImage imageNamed:@"ico_position"];
    [_back addSubview:_positionIcon];
    [_positionIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_carId.mas_bottom);
        make.left.mas_equalTo(_carId);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    }];

    // 地址
    _address = [UILabel new];
    _address.text = @"呵呵呵呵呵呵";
    [_back addSubview:_address];
    [_address mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_positionIcon.mas_centerY);
        make.left.mas_equalTo(_positionIcon.mas_right).offset(10);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(_carId);
    }];
    
    // 增值
    _moreServer = [UILabel new];
    _moreServer.text = @"增值服务";
    [_back addSubview:_moreServer];
    [_moreServer mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_address.mas_bottom);
        make.left.right.mas_equalTo(_carId);
        make.height.mas_equalTo(_carId);

    }];
    
    // 日期
    _time = [UILabel new];
    _time.text = @"过日子";
    [_back addSubview:_time];
    [_time mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_moreServer.mas_bottom);
        make.left.right.height.mas_equalTo(_carId);
    }];
    
    // 抢单按钮
    _plunder = [UIButton buttonWithType:UIButtonTypeSystem];
    _plunder.backgroundColor = MainBtnColor;
    [_plunder setTitle:@"抢单" forState:UIControlStateNormal];
    [_back addSubview:_plunder];
    [_plunder addTarget:self action:@selector(plunderAction:) forControlEvents:UIControlEventTouchUpInside];
    [_plunder mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_time.mas_bottom).offset(5);
        make.centerX.mas_equalTo(_back.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(KScreenWidth - 40, 30));
        
    }];
}

- (void)plunderAction:(UIButton *)btn{
    _block();
}

- (void)plunderComfirmWithBlock:(PlunderBlock)block{
    _block = block;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
