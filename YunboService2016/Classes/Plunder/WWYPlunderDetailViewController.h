//
//  WWYPlunderDetailViewController.h
//  YunboService2016
//
//  Created by 王维一 on 16/3/17.
//  Copyright © 2016年 CarService. All rights reserved.
//

#import "YHBaseViewController.h"
@class listModel;

@interface WWYPlunderDetailViewController : YHBaseViewController

@property (nonatomic, strong) listModel *listModel;

@end
