//
//  PlunderViewController.m
//  YunboService2016
//
//  Created by apple on 3/2/16.
//  Copyright © 2016 CarService. All rights reserved.
//

#import "PlunderViewController.h"
#import "WWYPlunderTableViewCell.h"
#import "WWYPlunderDetailViewController.h"

#define CELLHEIGHT 150

@interface PlunderViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UILabel *prompt;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UIAlertController *alert;

@property (nonatomic, strong) UIView *line;

@property (nonatomic, assign) BOOL isOreder;




@end

@implementation PlunderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
   
    //[self getData]; // 获取数据
    [self order];
    [self naviConfig];
    [self BMKLocation]; // 定位..貌似没什么用
    
    
    
}

- (void)naviConfig{
    self.sc_navigationBar.backgroundColor = [UIColor whiteColor];
    self.sc_navigationItem.title = @"抢单中心";
    
    _line = [UIView new];
    _line.backgroundColor = [UIColor grayColor];
    [self.view addSubview:_line];
    [_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(KTopHeight);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(1);
    }];
}

- (void)tableViewConfig{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 44 + 1, KScreenWidth, KScreenHeight - 44) style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.rowHeight = CELLHEIGHT;
    [self.view addSubview:_tableView];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"WWYPlunderTableViewCell";
    WWYPlunderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell){
        cell = [[WWYPlunderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
        [cell plunderComfirmWithBlock:^{
            [self confirmPlunder];
        }];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    WWYPlunderDetailViewController *detail = [[WWYPlunderDetailViewController alloc] init];
    [self.navigationController pushViewController:detail animated:YES];
    
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}



- (void)BMKLocation{
    
}

- (void)confirmPlunder{
    // 提示框
    _alert = [UIAlertController alertControllerWithTitle:@"云泊" message:@"请您核对抢单信息,是否抢单" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"确定抢单" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
       NSLog(@"确认抢单");
        
        
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"取消抢单");
    }];
    
   
    [_alert addAction:cancel];
    [_alert addAction:confirm];
    
    
    
    [self presentViewController:_alert animated:YES completion:nil];
    
    
    
}
#pragma- 多长时间应该请求一次
- (void)getData{
    NSDictionary *dic = @{@"service":@"Cleaner.getOrdersNearby",@"serverId":KgetUserValueByParaName(USERID), @"longitude":@"120.1196", @"latitude":@"30.29636"};
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:dic andHub:HubStyleNormal andBlocks:^(NSDictionary *result) {
//        NSLog(@"%@", result);
//        NSLog(@"订单判断方法");
//        NSLog(@"%@", [[result valueForKey:@"code"] class]);   // 多大仇  code是NSNumber类型的
        if ([[result valueForKey:@"code"]  isEqual: @(1)]) {
            // 没有订单
            NSLog(@"执行没有订单");
            [self noOrder];
        }else if([[result valueForKey:@"code"]  isEqual: @(0)]){
            // 获取成功
            NSLog(@"执行有订单");
            [self order];
        }
    }];
}

- (void)noOrder{
    _prompt = [UILabel new];
    _prompt.text = @"目前没有订单";
    _prompt.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:_prompt];
    [_prompt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 100, 0));
        
    }];
}
- (void)order{
    [_prompt  removeFromSuperview];
    [self tableViewConfig]; // 配置tableView;
    
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
//    NSDictionary *dic = @{@"service":@"Cleaner.login", @"mobile":@"13567168362", @"passwd":@"123456"};
//    [HTTPManager getDataWithUrl:MainAPI andPostParameters:dic andBlocks:^(NSDictionary *result) {
//        NSNumber *code = [result objectForKey:@"code"];
//        if ([code isEqualToNumber:@0]) {
//            [YHJHelp showMessage:[result objectForKey:@"message"]];
//        }else{
//            [YHJHelp showMessage:[result objectForKey:@"message"]];
//        }
//    }];
    
    
}
@end
