//
//  WWYPlunderTableViewCell.h
//  YunboService2016
//
//  Created by 王维一 on 16/3/17.
//  Copyright © 2016年 CarService. All rights reserved.
//

#import "YHBaseTableViewCell.h"

typedef void(^PlunderBlock)();

@interface WWYPlunderTableViewCell : YHBaseTableViewCell

@property (nonatomic, copy) PlunderBlock block;

- (void)plunderComfirmWithBlock:(PlunderBlock )block;

@end
