//
//  listModel.h
//  YunboService2016
//
//  Created by 王维一 on 16/3/15.
//  Copyright © 2016年 CarService. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface listModel : NSObject
// 订单号
@property (nonatomic, strong) NSString *orderId;
// 用户id
@property (nonatomic, strong) NSString *userId;
// 用户名
@property (nonatomic, strong) NSString *userName;
// 用户手机号
@property (nonatomic, strong) NSString *userMobile;
// 欠费
@property (nonatomic, strong) NSString *urgentFee;
// 用户签名
@property (nonatomic, strong) NSString *userNote;
// pNUmber
@property (nonatomic, strong) NSString *pNumber;
// 车牌号
@property (nonatomic, strong) NSString *carId;
// 地址
@property (nonatomic, strong) NSString *address;
// 经度
@property (nonatomic, strong) NSString *longitude;
// 纬度
@property (nonatomic, strong) NSString *latitude;
// 下单时间
@property (nonatomic, strong) NSString *orderTime;
// 开始时间
@property (nonatomic, strong) NSString *fromTime;
// .....
@property (nonatomic, strong) NSString *toTime;
// 花费时间
@property (nonatomic, strong) NSString *takeTime;
// 开始时间
@property (nonatomic, strong) NSString *startTime;
// 结束时间
@property (nonatomic, strong) NSString *finishTime;
// 付款时间
@property (nonatomic, strong) NSString *payTime;
// 支付状态
@property (nonatomic, strong) NSString *orderStatus;
// 备注
@property (nonatomic, strong) NSString *note;
// 是否支付
@property (nonatomic, strong) NSString *isPaid;
// 是否评价
@property (nonatomic, strong) NSString *isComment;
// 评分
@property (nonatomic, strong) NSString *commentScore;
// 增值服务
@property (nonatomic, strong) NSArray *moreService;






@end
