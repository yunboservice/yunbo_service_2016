//
//  WWYOrderDetailViewController.m
//  YunboService2016
//
//  Created by 王维一 on 16/3/11.
//  Copyright © 2016年 CarService. All rights reserved.
//

#import "WWYOrderDetailViewController.h"
#import "WWYChatViewController.h"
#import "listModel.h"
#import "MoreService.h"
#import "DemoGlobalClass.h"



@interface WWYOrderDetailViewController ()<BMKMapViewDelegate>

@property (nonatomic, strong) UILabel *carId;

@property (nonatomic, strong) UILabel *formToTime;

@property (nonatomic, strong) UIView *firstLine;

@property (nonatomic, strong) UILabel *moreService;

@property (nonatomic, strong) UILabel *remark;

@property (nonatomic, strong) UILabel *others;

@property (nonatomic, strong) UILabel *orderId;

@property (nonatomic, strong) UILabel *orderTime;

@property (nonatomic, strong) UILabel *address;

@property (nonatomic, strong) UILabel *startTime;

@property (nonatomic, strong) UILabel *isPay;

@property (nonatomic, strong) UIView *bottomLine;

// 百度地图定位
@property (nonatomic, strong) BMKMapView *map;

// 联系人信息
@property (nonatomic, strong) NSDictionary *dic;




@end


@implementation WWYOrderDetailViewController

- (void)dealloc{
    _map.delegate = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self naviConfig];
    [self getData];
    [self setupLayout];
}


- (void)naviConfig{
    self.sc_navigationItem.title = @"服务详情";
    self.sc_navigationBar.backgroundColor = [UIColor whiteColor];

    // 图标太丑(重新约束)
   SCBarButtonItem *rigthItem = [[SCBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"btn_message"] style:SCBarButtonItemStyleBordered handler:^(id sender) {
       NSLog(@"跳转及时通讯");
       // (session参数)
       //_dic = @{nameKey:_listModel.userName, phoneKey:_listModel.userMobile, imageKey:[UIImage imageNamed:@""]};
       WWYChatViewController *chatVC = [[WWYChatViewController alloc] initWithSessionId:@"18868837744"];//_dic[phoneKey]];
        [self.navigationController pushViewController:chatVC animated:YES];
       
       //建立账户
       
       
   }];
    [rigthItem.button mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.size.mas_equalTo(CGSizeMake(60, 32));
    }];
    self.sc_navigationItem.rightBarButtonItem = rigthItem;
    
    // 返回键太大
    SCBarButtonItem *back = self.sc_navigationItem.leftBarButtonItem;
    [back.button mas_updateConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(36, 33));
        
    }];
}

- (void)getData{
    
    
}

#pragma mark- 控件布局, 赋值
- (void)setupLayout{
    NSLog(@"1");
    // 车牌
    _carId = [UILabel new];
    _carId.backgroundColor = [UIColor redColor];
    [self.view addSubview:_carId];
    [_carId mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(KTopHeight);
        make.left.mas_equalTo(5);
        make.size.mas_equalTo(CGSizeMake(KScreenWidth * 2 / 5, 35));
    }];
    
    // 起止日期
    _formToTime = [UILabel new];
    _formToTime.backgroundColor = [UIColor greenColor];
    _formToTime.font = [UIFont systemFontOfSize:13];
    [self.view addSubview:_formToTime];
    [_formToTime mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_carId);
        make.right.mas_equalTo(-5);
        make.size.equalTo(_carId);
    }];
    
    // 华丽丽的分割线
    _firstLine = [UIView new];
    _firstLine.backgroundColor = [UIColor grayColor];
    [self.view addSubview:_firstLine];
    [_firstLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(_carId.mas_top).offset(1);
        make.right.left.mas_equalTo(0);
        make.height.mas_equalTo(1);
    }];
    
    // 增值服务
    _moreService = [UILabel new];
    _moreService.backgroundColor = [UIColor brownColor];
    [self.view addSubview:_moreService];
    [_moreService makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_carId.mas_bottom).offset(5);
        make.left.equalTo(_carId);
        make.right.equalTo(0);
        make.height.mas_equalTo(35);
    }];
    
    // 备注
    _remark = [UILabel new];
    _remark.backgroundColor = [UIColor magentaColor];
    [self.view addSubview:_remark];
    [_remark makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_moreService.mas_bottom).offset(10);
        make.left.mas_equalTo(_moreService);
        make.right.mas_equalTo(0);
        make.size.equalTo(_moreService);
    }];
    
    // 百度地图
    _map = [BMKMapView new];
    [self mapConfig]; //地图相关配置
    _map.backgroundColor = [UIColor grayColor];

    [self.view addSubview:_map];
    [_map mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_remark.mas_bottom).offset(10);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(175);
    }];
    
    // 其他(其他详情的label以此拘束)
    _others = [UILabel new];
    _others.text = @"  其他详情";
    _others.layer.borderWidth = 2;
    _others.layer.borderColor = [[UIColor grayColor] CGColor];
    [self.view addSubview:_others];
    [_others mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_map.mas_bottom).offset(10);
        make.left.mas_equalTo(-2);
        make.size.mas_equalTo(CGSizeMake(KScreenWidth + 4, 35));
    }];
    
    // 订单号
    _orderId = [UILabel new];
    _orderId.backgroundColor = [UIColor cyanColor];
    _orderId.text = @"订单号码 : ";
    _orderId.font = [UIFont systemFontOfSize:13];
    [self.view addSubview:_orderId];
    
    // 订单时间
    _orderTime = [UILabel new];
    _orderTime.text = @"订单时间 : ";
    _orderTime.font = [UIFont systemFontOfSize:13];
    _orderTime.backgroundColor = [UIColor cyanColor];
    [self.view addSubview:_orderTime];
   
    
    // 洗车地址
    _address = [UILabel new];
    _address.text = @"洗车地址 : ";
    _address.font = [UIFont systemFontOfSize:13];
    _address.backgroundColor = [UIColor cyanColor];
    [self.view addSubview:_address];
 
    
    // 开始时间
    _startTime = [UILabel new];
    _startTime.text = @"开始时间 : ";
    _startTime.font = [UIFont systemFontOfSize:13];
    _startTime.backgroundColor = [UIColor cyanColor];
    [self.view addSubview:_startTime];
    
    // 支付状态
    _isPay = [UILabel new];
    _isPay.text = @"支付状态 : ";
    _isPay.font = [UIFont systemFontOfSize:13];
    _isPay .backgroundColor = [UIColor cyanColor];
    [self.view addSubview:_isPay];
  
    // 整体布局(整体150高)
    [_orderId mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(KScreenWidth, 150 / 5));
        make.top.equalTo(_others.mas_bottom);
        make.left.mas_equalTo(10);
        
    }];
    
    [_orderTime mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(_orderId);
        make.top.equalTo(_orderId.mas_bottom);
        make.left.equalTo(_orderId);
    }];
    
    [_address mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(_orderId);
        make.top.equalTo(_orderTime.mas_bottom);
        make.left.equalTo(_orderId);
    }];
    
    [_startTime mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(_orderId);
        make.top.equalTo(_address.mas_bottom);
        make.left.equalTo(_orderId);
    }];
    
    [_isPay mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(_orderId);
        make.top.equalTo(_startTime.mas_bottom);
        make.left.equalTo(_orderId);
    }];
    
    // 下线
    _bottomLine = [UIView new];
    _bottomLine.backgroundColor = [UIColor grayColor];
    [self.view addSubview:_bottomLine];
    [_bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_isPay.mas_bottom);
        make.height.mas_equalTo(2);
        make.left.mas_equalTo(-2);
        make.right.mas_equalTo(2);
        
    }];
    
    
    
    //******************************************* 控件赋值 *********************************************************
    _carId.text = _listModel.pNumber;
#warning 时间有点问题
    // 拼接时间
    NSString *startTime = [[_listModel.startTime componentsSeparatedByString:@" "] lastObject];
                    
    NSString *formToTime = [[_listModel.toTime componentsSeparatedByString:@" "] lastObject];
    
    NSString *time = [NSString stringWithFormat:@"%@ - %@", startTime, formToTime];
    
    _formToTime.text = time;
    
    // 备注判断
    if (IsEmptyStr(_listModel.note)) {
        _remark.text = [NSString stringWithFormat:@"备注 : 无"];
    }else{
        _remark.text = [NSString stringWithFormat:@"备注 : %@", _listModel.note];
    }
    
    // 增值服务
    NSString *service = [NSString string]; // 服务名
    
    for (NSDictionary *temp in _listModel.moreService) {
        if (![[temp valueForKey:@"key"] isEqualToString:@"base"]){
            MoreService *more = [[MoreService alloc] init];
            [more setValuesForKeysWithDictionary:temp];
            
            service = [NSString stringWithFormat:@"%@ ", more.des];
        }
        
    }
    if (IsEmptyStr(service)){
        _moreService.text = @"增值服务 : 无";
    }else{
        _moreService.text = [NSString stringWithFormat:@"增值服务 : %@", service];
    }
    
    
    if (_listModel){
        _orderId.text = [_orderId.text stringByAppendingString: _listModel.orderId];
        
        _orderTime.text = [_orderTime.text stringByAppendingString: _listModel.orderTime];
        
        _address.text = [_address.text stringByAppendingString: _listModel.address];
        
        _startTime.text = [_startTime.text stringByAppendingString: _listModel.startTime];
        
        if ([_listModel.isPaid isEqualToString:@"1"]) {
            _isPay.text = @"已付款";
        }else{
            _isPay.text = @"未付款";
        }

    }
}

#pragma mark- 百度地图相关设置
- (void)mapConfig{
    _map.delegate = self;
    _map.zoomLevel = 15;
    
    //相关配置
    BMKPointAnnotation* annotation = [[BMKPointAnnotation alloc]init];
    CLLocationCoordinate2D coor;
    coor.latitude = [_listModel.latitude doubleValue];
    coor.longitude = [_listModel.longitude doubleValue];
    annotation.coordinate = coor;
    annotation.title = @"服务地点";
    [_map addAnnotation:annotation];
    
    //定位中心点
    BMKCoordinateRegion region ;//表示范围的结构体
    region.center = coor;//中心点
//    region.span.latitudeDelta = 0.1;//经度范围（设置为0.1表示显示范围为0.2的纬度范围）
//    region.span.longitudeDelta = 0.1;//纬度范围
    [_map setRegion:region animated:YES];
}

- (BMKAnnotationView *)mapView:(BMKMapView *)mapView viewForAnnotation:(id <BMKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[BMKPointAnnotation class]]) {
        BMKPinAnnotationView *newAnnotationView = [[BMKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"myAnnotation"];
        newAnnotationView.pinColor = BMKPinAnnotationColorPurple;
        newAnnotationView.animatesDrop = YES;// 设置该标注点动画显示
        
        //自定义覆盖物
        newAnnotationView.image = [UIImage imageNamed:@"service_point.png"];


        return newAnnotationView;
    }
    return nil;
}


// 属性的set方法
- (void)setListModel:(listModel *)listModel{
    if (_listModel != listModel) {
        _listModel = listModel;
    }

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end

