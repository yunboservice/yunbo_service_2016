//
//  WWYOrderDetailViewController.h
//  YunboService2016
//
//  Created by 王维一 on 16/3/11.
//  Copyright © 2016年 CarService. All rights reserved.
//

#import "YHBaseViewController.h"
@class listModel;

@interface WWYOrderDetailViewController : YHBaseViewController

@property (nonatomic, strong) listModel *listModel;

@end
