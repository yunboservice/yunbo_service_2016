//
//  WWYOrderListTableViewCell.h
//  YunboService2016
//
//  Created by 王维一 on 16/3/10.
//  Copyright © 2016年 CarService. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YHBaseTableViewCell.h"
@class listModel;

@interface WWYOrderListTableViewCell : YHBaseTableViewCell

@property (nonatomic, strong) listModel *model;

@end
