//
//  OrderListViewController.m
//  YunboService2016
//
//  Created by apple on 3/2/16.
//  Copyright © 2016 CarService. All rights reserved.
//

#import "OrderListViewController.h"
#import "WWYOrderListTableViewCell.h"
#import "WWYOrderDetailViewController.h"
#import "MoreService.h"
#import "listModel.h"

#define CELLHEIGHT 110

@interface OrderListViewController ()<UITableViewDataSource, UITableViewDelegate>{
    NSInteger pageIndex;
    NSInteger pageSize;
}

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *array;

@property (nonatomic, strong) UIView *line;



@end

@implementation OrderListViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor grayColor];
    //数据初始化
    pageIndex = 1;
    pageSize = 10;
    _array = [NSMutableArray array];
    
    [self setupNavi];
    [self tableViewConfig];
    [self getDataWithPageIndex:pageIndex andPageSize:pageSize];
}

// 导航栏设置
- (void)setupNavi{
    self.sc_navigationBar.backgroundColor = [UIColor whiteColor];
    self.sc_navigationItem.title = @"服务记录";
    
    _line = [UIView new];
    _line.backgroundColor = [UIColor grayColor];
    [self.view addSubview:_line];
    [_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(KTopHeight + 1);
        make.left.right.mas_equalTo(0);
        make.height.mas_equalTo(1);
    }];
    
}

// tableView相关设置
- (void)tableViewConfig{
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, KTopHeight + 1 , KScreenWidth, KScreenHeight - KTopHeight - 44) style:UITableViewStylePlain];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.rowHeight = CELLHEIGHT;
    
    
    // 注册cell
    [_tableView registerClass:[WWYOrderListTableViewCell class] forCellReuseIdentifier:@"orderCell"];
    
    [self.view addSubview:_tableView];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *identifier = @"orderCell";
    WWYOrderListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[WWYOrderListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.model = _array[indexPath.row];
    //取消点击cell的高亮状态
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

// 点击cell跳转详情
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    WWYOrderDetailViewController *detailVC = [[WWYOrderDetailViewController alloc] init];
    detailVC.listModel = _array[indexPath.row];
    [self.navigationController pushViewController:detailVC animated:YES];
    
    //取消选中转状态
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

/**
 *  获取数据
 *
 *  @param index 页码
 *  @param size 页数据量
 */
- (void)getDataWithPageIndex:(NSInteger )index andPageSize:(NSInteger )size{
    
    NSString *indexStr = [NSString stringWithFormat:@"%ld", index];
    NSString *sizeStr = [NSString stringWithFormat:@"%ld", size];
    
    // 加密
    NSDictionary *para = @{@"service":@"Cleaner.getOrdersRecord", @"serverId":KgetUserValueByParaName(USERID), @"pageIndex": indexStr, @"pageSize":sizeStr, };
    
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:para andHub:HubStyleNormal andBlocks:^(NSDictionary *result) {
        for (NSDictionary *temp in [[result valueForKey:@"orders"] valueForKey:@"list"]) {
            listModel *model = [[listModel alloc] init];
            [model setValuesForKeysWithDictionary:temp];
            
            // 处理数据, 刷新界面
            [_array addObject:model];
            [_tableView reloadData];
        }
        
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
