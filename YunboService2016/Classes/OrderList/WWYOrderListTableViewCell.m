//
//  WWYOrderListTableViewCell.m
//  YunboService2016
//
//  Created by 王维一 on 16/3/10.
//  Copyright © 2016年 CarService. All rights reserved.
//

#import "WWYOrderListTableViewCell.h"
#import "listModel.h"
#import "MoreService.h"
#import "YHJHelp.h"

@interface WWYOrderListTableViewCell ()

@property (nonatomic, strong) UIView *cellBack;

@property (nonatomic, strong) UILabel *carID;

@property (nonatomic, strong) UILabel *server;

@property (nonatomic, strong) UIImageView *positionIron;

@property (nonatomic, strong) UILabel *position;

@property (nonatomic, strong) UIView *bottomBack;

@property (nonatomic, strong) UILabel *fee;

@property (nonatomic, strong) UILabel *isPay;




@end

@implementation WWYOrderListTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
   self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self setupCell];
    }
    return self;
}

- (void)setupCell{
    // 背景
    _cellBack = [UIView new];
    _cellBack.backgroundColor = [UIColor grayColor];
    _cellBack.frame = CGRectMake(5, 0, KScreenWidth - 10, 100);
    [YHJHelp setMaskTo:_cellBack byRoundingCorners:UIRectCornerAllCorners WithRadii:5.0f];
    [self.contentView addSubview:_cellBack];
//    [self.contentView addSubview:_cellBack];
//       [_cellBack mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(0);
//        make.left.mas_equalTo(5);
//        make.right.mas_equalTo(-5);
//        make.height.mas_equalTo(100);
//        
//    }];
    
    // 车牌
    _carID = [UILabel new];
    _carID.backgroundColor = [UIColor redColor];
    _carID.text = @"测试车牌";
    [_cellBack addSubview:_carID];
    [_carID mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(0);
        make.left.equalTo(5);
        make.width.mas_equalTo(KScreenWidth / 3);
        make.height.equalTo(30);
        
    }];
    
    // 增值服务
    _server = [UILabel new];
    _server.backgroundColor = [UIColor greenColor];
    [_cellBack addSubview:_server];
    _server.text = @"测试增值服务";
    [_server mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_carID);
        make.left.equalTo(_carID.mas_right).offset(30);
        make.right.equalTo (0);
        make.height.equalTo(_carID);
    }];
    
    // 地址小图标
    _positionIron = [UIImageView new];
    _positionIron.image = [UIImage imageNamed:@"ico_position"];
    _positionIron.contentMode = UIViewContentModeScaleAspectFit;
    [_cellBack addSubview:_positionIron];
    [_positionIron mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_carID);
        make.top.equalTo(_carID.mas_bottom).offset(10);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    }];
    
    // 地址
    _position = [UILabel new];
    _position.backgroundColor = [UIColor brownColor];
    _position.text = @"杭州市xxxxxxxxxxxx";
    [_cellBack addSubview:_position];
    [_position mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_positionIron.mas_right);
        make.right.equalTo(0);
        make.centerY.equalTo(_positionIron.mas_centerY);
        make.height.mas_equalTo(30);
    }];
    
    // 底部背景
    _bottomBack = [UIView new];
    _bottomBack.backgroundColor = [UIColor grayColor];
    [_cellBack addSubview:_bottomBack];
    [_bottomBack mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_position.mas_bottom).offset(5);
        make.bottom.mas_equalTo(-10);
        make.right.equalTo(1);
        make.left.equalTo(-1);
    }];

    // 记费
    _fee = [UILabel new];
    _fee.backgroundColor = [UIColor magentaColor];
    _fee.text = @"好多钱";
    [_bottomBack addSubview:_fee];
    [_fee mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(5);
        make.left.equalTo(_carID);
        make.width.mas_equalTo(_carID);
        make.bottom.mas_equalTo(-5);
    }];

    // 支付状态
    _isPay = [UILabel new];
    _isPay.backgroundColor = [UIColor cyanColor];
    _isPay.text = @"支付状态";
    [_bottomBack addSubview:_isPay];
    [_isPay mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_fee.mas_top);
        make.right.mas_equalTo(-5);
        make.width.equalTo(_fee);
        make.height.equalTo(_fee.mas_height);
    }];
}

// 控件赋值
- (void)setModel:(listModel *)model{
    if (_model != model){
        _model = model;
    }
    _carID.text = _model.carId;
    // 增值服务
    NSString *service = [NSString string]; // 服务名
    float count; // 总价
    
    for (NSDictionary *temp in _model.moreService) {
        MoreService *more = [[MoreService alloc] init];
        [more setValuesForKeysWithDictionary:temp];
        
        //计算总价
        float fee = [more.fee floatValue];
        count += fee;
        
        service =  [NSString stringWithFormat:@"%@  ",more.des];// 拼接服务
        
    }
    _server.text = service;
    
    _position.text = _model.address;
    
    _fee.text = [NSString stringWithFormat:@"记费: %0.2f", count];
    
    
    // 判断支付状态
    if ([_model.isPaid isEqualToString:@"1"]){
        _isPay.text = @"已付款";
    }else{
        _isPay.text = @"未付款";
    }
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
