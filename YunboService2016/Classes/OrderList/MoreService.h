//
//  MoreService.h
//  YunboService2016
//
//  Created by 王维一 on 16/3/15.
//  Copyright © 2016年 CarService. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MoreService : NSObject

@property (nonatomic, strong) NSString *key;

@property (nonatomic, strong) NSString *fee;

@property (nonatomic, strong) NSString *des;

@property (nonatomic, strong) NSString *abbr;


@end
