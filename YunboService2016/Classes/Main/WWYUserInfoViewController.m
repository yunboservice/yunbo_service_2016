//
//  WWYUserInfoViewController.m
//  YunboService2016
//
//  Created by 王维一 on 16/3/22.
//  Copyright © 2016年 CarService. All rights reserved.
//

#import "WWYUserInfoViewController.h"
#import "WWYUserHeaderView.h"
#import "CleanerInfo.h"

@interface WWYUserInfoViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) UITableView *tableView;

@property (strong, nonatomic) WWYUserHeaderView *header;

@property (strong, nonatomic) UIButton *logOut;

@property (strong, nonatomic) CleanerInfo *cleaner;

@property (strong, nonatomic) NSMutableArray *promptArray;


@end

@implementation WWYUserInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self getData];
    [self setUpLayout];
    [self naviConfig];

                     
}


- (void)setUpLayout{
    NSLog(@"用户Controller布局");
    _header = [WWYUserHeaderView viewWithHeight:150.f andBackImage:[UIImage imageNamed:@"d8e9b9ef1a93f4383d20923ea77026e6.jpeg"] andUserHeader:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_cleaner.photo]]] andUserName:@"yubo"];
    [_header userHeaderBtnAction:^{
        NSLog(@"用户界面 调用系统相册");
    }];
    [self.view addSubview:_header];
        
    [self tableViewConfig];
}

// 获取数据, 赋值
- (void)getData{
    NSDictionary *dic = @{@"service":@"Cleaner.getBaseInfo", @"serverId":KgetUserValueByParaName(USERID)};
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:dic andHub:HubStyleNormal andBlocks:^(NSDictionary *result) {
        NSLog(@"%@", [result valueForKey:@"info"]);
        _cleaner = [[CleanerInfo alloc] init];
        [_cleaner setValuesForKeysWithDictionary:[result valueForKey:@"info"]];
        
        _header.userHeader = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_cleaner.photo]]];
        _header.userName = _cleaner.nickname;
        UITableViewCell *address = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        address.detailTextLabel.text = _cleaner.address;
        
        UITableViewCell *photoNumber = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        photoNumber.detailTextLabel.text = _cleaner.mobile;
        
        
        
    }];
}
     
- (void)naviConfig{
    self.sc_navigationItem.title = @"个人信息";
    self.sc_navigationBar.backgroundColor = [UIColor whiteColor];
    
    SCBarButtonItem *btn = self.sc_navigationItem.leftBarButtonItem;
    [btn.button mas_updateConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(35, 35));
        
    }];
}

- (void)tableViewConfig{
    _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    [self.view addSubview:_tableView];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_header.mas_bottom);
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(KTabBarHeight + KTopHeight + 100);
    }];

    
    // 尾区
    _tableView.tableFooterView = [UIButton buttonWithType:UIButtonTypeSystem];
    _tableView.tableFooterView.height = 40;
    _tableView.tableFooterView.backgroundColor = [UIColor cyanColor];
    
    
    // 登出按钮
    _logOut = [UIButton buttonWithType:UIButtonTypeSystem];
    _logOut.backgroundColor = [UIColor redColor];
    _logOut.tintColor = MainTextColor;
    [_logOut setTitle:@"退出登录" forState:UIControlStateNormal];
    [_logOut addTarget:self action:@selector(logOutAction:) forControlEvents:UIControlEventTouchUpInside];
    [_tableView.tableFooterView addSubview:_logOut];
    [_logOut mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.mas_equalTo(0);
        make.centerX.mas_equalTo(_tableView.mas_centerX);
        make.width.mas_equalTo(220);
    }];
    
    
    //显示
    _promptArray = @[@"地址", @"电话", @"修改密码", @"二维码名片", @"月洗车统计", @"投诉单查询"].mutableCopy;

    
    

}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 4;
    }else{
        return 2;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{   if (section == 0){
        return 0.1;
    }else{
        return 10;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0 && indexPath.row <= 1){
        static NSString *identiifer = @"userInfoCellValue1";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"userInfoCellValue1"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identiifer];
            cell.textLabel.text = _promptArray[indexPath.section + indexPath.row];
            

        }
            return cell;
    }else{
        static NSString *identifier = @"userInfoCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"userInfoCell"];
        if (!cell){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.textLabel.text = _promptArray[indexPath.section * 4 + indexPath.row];

        }
            return cell;
    }
 
}
- (void)logOutAction:(UIButton *)btn{
    NSLog(@"执行登出逻辑");
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:@{@"service":@"Cleaner.logout", @"serverId":KgetUserValueByParaName(USERID)} andHub:HubStyleNormal andBlocks:^(NSDictionary *result) {
        MyLog(@"%@", result);
    }];
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
