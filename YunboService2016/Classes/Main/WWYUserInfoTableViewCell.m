//
//  WWYUserInfoTableViewCell.m
//  YunboService2016
//
//  Created by 王维一 on 16/3/23.
//  Copyright © 2016年 CarService. All rights reserved.
//

#import "WWYUserInfoTableViewCell.h"

@interface WWYUserInfoTableViewCell ()

@property (nonatomic, strong) UILabel *title;

@property (nonatomic, strong) UILabel *detail;

@end



@implementation WWYUserInfoTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self layoutCell];
    }
    return self;
}

- (void)layoutCell{
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
