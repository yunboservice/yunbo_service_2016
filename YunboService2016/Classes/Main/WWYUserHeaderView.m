//
//  WWYUserHeaderView.m
//  YunboService2016
//
//  Created by 王维一 on 16/3/22.
//  Copyright © 2016年 CarService. All rights reserved.
//

#import "WWYUserHeaderView.h"

@interface WWYUserHeaderView ()

@property (strong, nonatomic) UIImageView *backView;

@property (strong, nonatomic) UIButton *headerBtn;

@property (strong, nonatomic) UILabel *nameLabel;



@end


@implementation WWYUserHeaderView

- (instancetype)initWithFrame:(CGRect)frame andHeight:(CGFloat)height andBackImage:(UIImage *)back andUserHeader:(UIImage *)header andUserName:(NSString *)name
{
    self = [super initWithFrame:frame];
    if (self) {
        [self viewSetUpWithHeight:height andBackImage:back andUserHeader:header andUserName:name];
    }
    return self;
}

+ (instancetype)viewWithHeight:(CGFloat)height andBackImage:(UIImage *)back andUserHeader:(UIImage *)header andUserName:(NSString *)name {

    WWYUserHeaderView *view = [[WWYUserHeaderView alloc] initWithFrame:CGRectMake(0, KTopHeight, KScreenWidth, height) andHeight:height andBackImage:back andUserHeader:header andUserName:name] ;
    
    return view;
}

- (void)viewSetUpWithHeight:(CGFloat)height andBackImage:(UIImage *)back andUserHeader:(UIImage *)header andUserName:(NSString *)name {
    NSLog(@"走了 %f", self.bounds.size.height);

    // 图片显示有问题
    _backView = [[UIImageView alloc] initWithFrame:self.bounds];
    _backView.contentMode = UIViewContentModeScaleAspectFill;
    _backView.clipsToBounds = YES;
   _backView.backgroundColor = [UIColor greenColor];
    _backView.image = back;

    [self addSubview:_backView];
    
    // 用户头像
    _headerBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    _headerBtn.backgroundColor = [UIColor clearColor];
    [_headerBtn addTarget:self action:@selector(headerBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [_headerBtn setImage:header forState:UIControlStateNormal];
    [self addSubview:_headerBtn];
    [_headerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(20);
        make.centerX.mas_equalTo(self.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(80, 80));
    }];
    
    // 用户名
    _nameLabel = [UILabel new];
    //_nameLabel.backgroundColor = [UIColor redColor];
    _nameLabel.textColor = [UIColor whiteColor];
    _nameLabel.textAlignment = NSTextAlignmentCenter;
    _nameLabel.text = _userName;
    [self addSubview:_nameLabel];
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_headerBtn.mas_bottom).offset(10);
        make.centerX.mas_equalTo(_headerBtn.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(200, 30));
        
    }];
}

- (void)userHeaderBtnAction:(btnBlock)block{
    _block = block;
}
// 触发点击事件
- (void)headerBtnAction:(UIButton *)btn{
    _block();
}


- (void)setUserHeader:(UIImage *)userHeader{
    [_headerBtn setImage:userHeader forState:UIControlStateNormal];
}

- (void)setUserName:(NSString *)userName{
    _nameLabel.text = userName;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
