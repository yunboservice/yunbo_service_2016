//
//  WWYUserHeaderView.h
//  YunboService2016
//
//  Created by 王维一 on 16/3/22.
//  Copyright © 2016年 CarService. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^btnBlock)();

@interface WWYUserHeaderView : UIView

@property (strong, nonatomic) UIImage *backImage;

@property (strong, nonatomic) UIImage *userHeader;

@property (copy, nonatomic) NSString *userName;

@property (copy, nonatomic) btnBlock block;



/**
 *  用户界面 头视图 默认全屏宽度
 *
 *  @param height    自定义高度
 *  @param back      背景图片
 *  @param header    用户头像
 *  @param name      用户名
 *
 *  @return 视图
 */
+ (instancetype)viewWithHeight:(CGFloat)height andBackImage:(UIImage *)back andUserHeader:(UIImage *)header andUserName:(NSString *)name;


/**
 *  点击用户头像按键
 *
 *  @param block 点击事件回调
 */
- (void)userHeaderBtnAction:(btnBlock)block;



@end
