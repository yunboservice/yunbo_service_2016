//
//  DBDataModel.m
//  YunboService2016
//
//  Created by 王维一 on 16/4/1.
//  Copyright © 2016年 CarService. All rights reserved.
//

#import "DBDataModel.h"

@implementation DBDataModel

+ (instancetype)strandModel{
    static DBDataModel *model;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        model = [[DBDataModel alloc] init];
    });
    
    return model;
}

@end
