//
//  AssociatorModel.h
//  CarService
//
//  Created by apple on 15/8/13.
//  Copyright (c) 2015年 fenglun. All rights reserved.
//

#import "YHBaseModel.h"
@interface AssociatorModel : YHBaseModel

@property (nonatomic ,copy) NSString *voipAccount;/**<容联账号*/
@property (nonatomic ,copy) NSString *userId;/**<用户ID*/
@property (nonatomic, copy) NSString *nickName;/**<用户昵称*/
@property (nonatomic ,copy) NSString *photo;/**<头像*/
@property (nonatomic ,copy) NSString  *type;/**<类型 1 用户账号  2服务者账号 3.数组*/
@property (nonatomic, copy) NSString *subAccount;/**<视频聊天账号*/

@end