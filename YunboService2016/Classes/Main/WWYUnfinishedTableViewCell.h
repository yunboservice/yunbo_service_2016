//
//  WWYUnfinishedTableViewCell.h
//  YunboService2016
//
//  Created by 王维一 on 16/3/24.
//  Copyright © 2016年 CarService. All rights reserved.
//

#import "YHBaseTableViewCell.h"
@class listModel;

typedef void(^unfinishedCellCheckBtnBlock)();

@interface WWYUnfinishedTableViewCell : YHBaseTableViewCell

@property (nonatomic, strong) listModel *lisModel;

@property (nonatomic, copy) unfinishedCellCheckBtnBlock BtnBlock;


- (void)unfinishedCellCheckBtnBlock:(unfinishedCellCheckBtnBlock)block;


@end
