//
//  WWYScanViewController.m
//  YunboService2016
//
//  Created by 王维一 on 16/3/21.
//  Copyright © 2016年 CarService. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "WWYScanViewController.h"



@interface WWYScanViewController ()<AVCaptureMetadataOutputObjectsDelegate>

@property ( strong , nonatomic ) AVCaptureDevice * device;
@property ( strong , nonatomic ) AVCaptureDeviceInput * input;
@property ( strong , nonatomic ) AVCaptureMetadataOutput * output;
@property ( strong , nonatomic ) AVCaptureSession * session;
@property ( strong , nonatomic ) AVCaptureVideoPreviewLayer * preview;

// 黑色背景
@property (strong, nonatomic) UIView *top;
@property (strong, nonatomic) UIView *left;
@property (strong, nonatomic) UIView *right;
@property (strong, nonatomic) UIView *bottom;

// 扫码动画
@property (strong, nonatomic) UIView *scanf;
@property (strong, nonatomic) UIView *line;

// 提示语
@property (nonatomic, nonnull) UILabel *prompt;


// 照明按钮




@end

@implementation WWYScanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self naviConfig];
    [self initialtion];
    [self backSetup];
}

- (void)naviConfig{
    // 返回键太大
    SCBarButtonItem *back = self.sc_navigationItem.leftBarButtonItem;
    [back.button mas_updateConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(35, 35));
        
    }];

    self.sc_navigationItem.title = @"扫码上班";
    
}
- (void)initialtion{
    NSLog(@"走了");
    NSError *error;
    // 获取摄像头
    _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    // 输入流
    _input = [AVCaptureDeviceInput deviceInputWithDevice:_device error:&error];

    if(!_input){
        NSLog(@"%@", [error localizedDescription]);
    }
    
    // 输出
    _output = [[ AVCaptureMetadataOutput alloc ] init ];
    
    [_output setMetadataObjectsDelegate : self queue : dispatch_get_main_queue ()]; // 签代理
    [ _output setRectOfInterest : CGRectMake (( 100 + KTopHeight )/ KScreenHeight ,(( KScreenWidth - 220 )/ 2 )/ KScreenWidth , 220 / KScreenHeight , 220 / KScreenWidth )]; // 扫码区为220, 220矩形, 距状态栏100
    
    // 会话
    _session = [[ AVCaptureSession alloc ] init ];
    [ _session setSessionPreset : AVCaptureSessionPresetHigh ];
    if ([ _session canAddInput : self . input ])
    {
        [ _session addInput : self . input ];
    }
    if ([ _session canAddOutput : self . output ])
    {
        [ _session addOutput : self . output ];
    }
    
    // 条码类型 AVMetadataObjectTypeQRCode
    _output . metadataObjectTypes = @[ AVMetadataObjectTypeQRCode ] ;
    
    // Preview
    _preview =[ AVCaptureVideoPreviewLayer layerWithSession : _session ];
    _preview . videoGravity = AVLayerVideoGravityResizeAspectFill ;
    _preview . frame = self . view . layer . bounds ;
    [ self . view . layer insertSublayer : _preview atIndex : 0 ];
    
    // 开始
    [_session startRunning];
}

#pragma mark AVCaptureMetadataOutputObjectsDelegate

// 返回结果
-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects
      fromConnection:(AVCaptureConnection *)connection
{
    NSLog(@"heheda");
    if (metadataObjects != nil && [metadataObjects count] > 0) {
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        NSString *result;
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
            result = metadataObj.stringValue;
        } else {
            NSLog(@"不是二维码");
        }
        [self performSelectorOnMainThread:@selector(reportScanResult:) withObject:result waitUntilDone:NO];
    }
}


- (void)reportScanResult:(NSString *)str{
    if (NOEmptyStr(str)) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    NSLog(@"%@", str);
}

// 停止扫码
- (void)stopReading
{
    // 停止会话
    [_session stopRunning];
    _session = nil;
}

// 背景布局
- (void)backSetup{
    _bottom = [UIView new];
    _bottom.backgroundColor = [UIColor blackColor];
    _bottom.alpha = 0.5;
    [self.view addSubview:_bottom];
    [_bottom mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(100 + KTopHeight + 220);
        make.bottom.mas_equalTo(0);
        make.left.right.mas_equalTo(0);
    }];
    
    
    _left = [UIView new];
    _left.backgroundColor = [UIColor blackColor];
    _left.alpha = 0.5;
    [self.view addSubview:_left];
    [_left mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.width.mas_equalTo((KScreenWidth - 220) / 2);
        make.bottom.mas_equalTo(_bottom.mas_top);
    }];
    
    _right = [UIView new];
    _right.backgroundColor = [UIColor blackColor];
    _right.alpha = 0.5;
    [self.view addSubview:_right];
    [_right mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_left);
        make.left.mas_equalTo(_left.mas_right).offset(220);
        make.bottom.mas_equalTo(_left);
        make.width.mas_equalTo(_left);
    }];
    
    _top = [UIView new];
    _top.backgroundColor = [UIColor blackColor];
    _top.alpha = 0.5;
    [self.view addSubview:_top];
    [_top mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(100 + KTopHeight);
        make.left.mas_equalTo(_left.mas_right);
        make.right.mas_equalTo(_right.mas_left);
        
    }];
    
    _scanf = [UIView new];
    [self.view addSubview:_scanf];
    [_scanf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_top.mas_bottom);
        make.left.mas_equalTo(_left.mas_right);
        make.size.mas_equalTo(CGSizeMake(220, 220));
    }];
    
    _line = [UIView new];
    _line.backgroundColor = [UIColor greenColor];
    [self.view addSubview:_line];
    [_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(180, 1));
        make.top.mas_equalTo(_scanf.mas_top);
        make.centerX.mas_equalTo(_scanf.mas_centerX);
    }];
    
    // 提示语
    _prompt = [[UILabel alloc] init];
    _prompt.text = @"请将二维码放在框内, 即可自动扫描";
    _prompt.textColor = [UIColor whiteColor];
    _prompt.textAlignment = NSTextAlignmentCenter;
    _prompt.font = [UIFont systemFontOfSize:13];
    [_bottom addSubview:_prompt];
    [_prompt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_bottom.mas_top).offset(10);
        make.size.mas_equalTo(CGSizeMake(220, 30));
        make.centerX.mas_equalTo(_bottom.mas_centerX);
    }];

    
    
}

// 系统照明
- (void)systemLightSwitch:(BOOL)open
{
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if ([device hasTorch]) {
        [device lockForConfiguration:nil];
        if (open) {
            [device setTorchMode:AVCaptureTorchModeOn];
        } else {
            [device setTorchMode:AVCaptureTorchModeOff];
        }
        [device unlockForConfiguration];
    }
}


- (void)viewDidLayoutSubviews{
    // 0.4秒前有个小bug;
    [UIView animateKeyframesWithDuration:3 delay:0.4 options:UIViewKeyframeAnimationOptionRepeat animations:^{
        _line.frame = CGRectMake(_line.frame.origin.x, _line.frame.origin.y + 219, _line.frame.size.width, _line.frame.size.height);
    } completion:^(BOOL finished) {
        
        
    }];
    
  

}








- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
