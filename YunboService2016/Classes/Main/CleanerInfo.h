//
//  CleanerInfo.h
//  YunboService2016
//
//  Created by 王维一 on 16/3/22.
//  Copyright © 2016年 CarService. All rights reserved.
//

#import "YHBaseModel.h"

@interface CleanerInfo : YHBaseModel
// 昵称
@property (copy, nonatomic) NSString *nickname;
// 性别
@property (copy, nonatomic) NSString *sex;
// 照片
@property (copy, nonatomic) NSString *photo;
// 电话号
@property (copy, nonatomic) NSString *mobile;
// id号
@property (copy, nonatomic) NSString *idNumber;
// 真实姓名
@property (copy, nonatomic) NSString *realName;
// 银行
@property (copy, nonatomic) NSString *bankName;
// 银行卡号
@property (copy, nonatomic) NSString *bankNumber;
// 地址
@property (copy, nonatomic) NSString *address;
// 社区id
@property (copy, nonatomic) NSString *communityId;
// 账单
@property (copy, nonatomic) NSString *account;
// 评分
@property (copy, nonatomic) NSString *score;
// 上工状态
@property (copy, nonatomic) NSString *isOnWork;
// 设备号
@property (copy, nonatomic) NSString *deviceId;


@property (copy, nonatomic) NSString *offWorkTime;

@property (copy, nonatomic) NSString *onWorkTime;

@end
