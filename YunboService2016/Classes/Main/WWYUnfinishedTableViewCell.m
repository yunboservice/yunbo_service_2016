//
//  WWYUnfinishedTableViewCell.m
//  YunboService2016
//
//  Created by 王维一 on 16/3/24.
//  Copyright © 2016年 CarService. All rights reserved.
//

#import "WWYUnfinishedTableViewCell.h"
#import "YHJHelp.h"
#import "WWYOrderDetailViewController.h"


@interface WWYUnfinishedTableViewCell ()

@property (nonatomic, strong) UIView *back;

@property (nonatomic, strong) UILabel *carId;

@property (nonatomic, strong) UILabel *moreService;

@property (nonatomic, strong) UIImageView *positionIcon;

@property (nonatomic, strong) UILabel *address;

@property (nonatomic, strong) UILabel *remark;

@property (nonatomic, strong) UILabel *note;

@property (nonatomic, strong) UIButton *detail;






@end

@implementation WWYUnfinishedTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setLayout];
    }
    return self;
}

- (void)setLayout{
    
    _back = [UIView new];
    _back.backgroundColor = [UIColor grayColor];
    _back.frame = CGRectMake(5, 0, KScreenWidth - 10, 140);
    [self.contentView addSubview:_back];
    [YHJHelp setMaskTo:_back byRoundingCorners:UIRectCornerAllCorners WithRadii:5.0f];
    [self.contentView addSubview:_back];
    
    _carId = [UILabel new];
    _carId.text = @"测试车牌";
    [_back addSubview:_carId];
    [_carId makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(5);
        make.size.mas_equalTo(CGSizeMake(150, 30));

    }];
    
    _moreService = [UILabel new];
    _moreService.text = @"测试服务状态";
    [_back addSubview:_moreService];
    [_moreService mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_carId);
        make.right.mas_equalTo(-5);
        make.size.equalTo(_carId);
    }];
    
    _positionIcon = [UIImageView new];
    [_back addSubview:_positionIcon];
    _positionIcon.image = [UIImage imageNamed:@"ico_position.png"];
    [_positionIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_carId.mas_bottom).offset(5);
        make.left.mas_equalTo(_carId);
        make.size.mas_equalTo(CGSizeMake(15, 15));
    }];
    
    _address = [UILabel new];
    _address.text = @"测试地址";
    _address.backgroundColor = [UIColor magentaColor];
    [_back addSubview:_address];
    [_address mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_positionIcon.mas_right).offset(10);
        make.centerY.mas_equalTo(_positionIcon.mas_centerY);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(30);
    }];
    
    _remark = [UILabel new];
    [_back addSubview:_remark];
    _remark.text = @"备注";
    [_remark mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_positionIcon.mas_bottom).offset(5);
        make.left.mas_equalTo(_positionIcon);
        make.size.mas_equalTo(CGSizeMake(50, 30));
        
    }];
    
    _note = [UILabel new];
    _note.text = @"测试备注";
    [_back addSubview:_note];
    [_note mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_remark);
        make.left.mas_equalTo(_remark.mas_right);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(_remark);
    }];
    
    _detail = [UIButton buttonWithType:UIButtonTypeSystem];
    _detail.backgroundColor = MainBtnColor;
    [_detail addTarget:self action:@selector(serverDetailAction:) forControlEvents:UIControlEventTouchUpInside];
    [_detail setTitle:@"服务详情" forState:UIControlStateNormal];
    [_back addSubview:_detail];
    [_detail mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_remark.mas_bottom).offset(5);
        make.centerX.mas_equalTo(_back.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(150, 35));
    }];
    
    
    
}

- (void)unfinishedCellCheckBtnBlock:(unfinishedCellCheckBtnBlock)block{
    _BtnBlock = block;
}

- (void)serverDetailAction:(UIButton *)btn{
    NSLog(@"未完成服务");
    _BtnBlock();
}


- (void)configWithModel:(id)cellModel{
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
