//
//  MainViewController.m
//  YunboService2016
//
//  Created by apple on 3/2/16.
//  Copyright © 2016 CarService. All rights reserved.
//

#import "MainViewController.h"
#import "YHJHelp.h"
#import "YHNavigationBar.h"
#import "DIYPickView.h"
#import "WWYScanViewController.h"
#import "WWYUserInfoViewController.h"
#import "listModel.h"
#import "ArrayDataSource.h"
#import "WWYUnfinishedTableViewCell.h"
#import "WWYOrderDetailViewController.h"
#import "DBDataModel.h"
#import "DemoGlobalClass.h"
#import "RlAccountModel.h"
#import "DeviceDelegateHelper.h"

#define CELLHIEGTH 150


@interface MainViewController ()<DIYPickViewDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UIView *lifetimeBack;

@property (nonatomic, strong) UIButton *advance;

@property (nonatomic, strong) UIButton *extend;

@property (nonatomic, strong) UILabel *workLifetime;

@property (nonatomic, strong) NSString *goToWork;

@property (nonatomic, strong) NSString *getOffWork;

@property (nonatomic, strong) DIYPickView *advancePick;

@property (nonatomic, strong) DIYPickView *extendPick;

// 跳转
@property (nonatomic, strong) WWYUserInfoViewController *user;

// 有无订单标志位
@property (nonatomic, assign) BOOL isOreder;

// 提示
@property (nonatomic, strong) UILabel *prompt;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSArray *listsArray;





@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor lightGrayColor];
    _goToWork = @"8:00";
    _getOffWork = @"19:00";
    
    [self naviConfig];
    [self setupLayout];
    [self getData];
    [self tableViewConfig];

    
    
}

// 导航栏设置
- (void)naviConfig{
    self.sc_navigationItem.title = [NSString stringWithFormat:@"云泊%@", [[NSUserDefaults standardUserDefaults] objectForKey:USERID]];
    self.sc_navigationBar.backgroundColor = [UIColor whiteColor];
    
    // 上班按钮
    SCBarButtonItem *rigthBtn = [[SCBarButtonItem alloc] initWithTitle:@"上班" style:SCBarButtonItemStyleBordered handler:^(id sender) {
        // 点击方法执行
        WWYScanViewController *scan = [[WWYScanViewController alloc] init];
        [self.navigationController pushViewController:scan animated:YES];
        
        KsetUserValueByParaName(@(1), USERSTATE);
    }];
    rigthBtn.button.backgroundColor = [UIColor redColor];
    // 重新约束rigthBtn宽度(默认48)
    [rigthBtn.button updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(35);
        make.width.mas_equalTo(85);
        
    }];
    [self.sc_navigationItem setRightBarButtonItem:rigthBtn];

    // 用户设置
    SCBarButtonItem *leftBtn = [[SCBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"your_photo.png"] style:SCBarButtonItemStyleBordered handler:^(id sender) {
        // 点击方法执行
        _user = [[WWYUserInfoViewController alloc] init];
        [self.navigationController pushViewController:_user animated:YES];
        
    }];
    [leftBtn.button updateConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo(CGSizeMake(44, 44));
    }];
    [self.sc_navigationItem setLeftBarButtonItem:leftBtn];
}
// 数据请求, 判断状态(订单的有无)
- (void)getData{
    
    [HTTPManager getDataWithUrl:MainAPI andPostParameters:@{@"service":@"Cleaner.getUnfinished", @"serverId":KgetUserValueByParaName(USERID)} andHub:HubStyleNormal andBlocks:^(NSDictionary *result) {
        MyLog(@"get data  %@",result);
        
        if ([[result valueForKey:@"code"] isEqualToNumber:@(0)]) {
            NSLog(@"有订单状态");
            _prompt = nil;
            //[self tableViewConfig];
        }else if([[result valueForKey:@"code"] isEqualToNumber:@(1)]){
            NSLog(@"没订单状态");
            //[self noOrder];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self tableViewConfig];
            });
            
        }
    }];
    
}
// self.View的布局
- (void)setupLayout{
    // 上班时间背景
    _lifetimeBack = [UIView new];
    _lifetimeBack.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_lifetimeBack];
    [_lifetimeBack mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0).offset(KTopHeight + 1);
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(40);
    }];
    
    // 提前上班按钮
    _advance = [UIButton buttonWithType:UIButtonTypeSystem];
    [_advance setTitle:@"提前上班" forState:UIControlStateNormal];
    _advance.backgroundColor = [UIColor orangeColor];
    [_advance addTarget:self action:@selector(advanceAction:) forControlEvents:UIControlEventTouchUpInside];
    [_lifetimeBack addSubview:_advance];
    [_advance mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.height.mas_equalTo(_lifetimeBack);
        make.width.mas_equalTo(100);
    }];
    
    // 延长下班按钮
    _extend = [UIButton buttonWithType:UIButtonTypeSystem];
    [_extend setTitle:@"延长下班" forState:UIControlStateNormal];
    _extend.backgroundColor = [UIColor orangeColor];
    [_extend addTarget:self action:@selector(extendAction:) forControlEvents:UIControlEventTouchUpInside];
    [_lifetimeBack addSubview:_extend];
    [_extend mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(_lifetimeBack);
        make.width.mas_equalTo(100);
    }];
    
    
    // 上班时间提示
    _workLifetime = [UILabel new];
    [_lifetimeBack addSubview:_workLifetime];
    _workLifetime.text = @"8:00 - 19:00";
    _workLifetime.textAlignment = NSTextAlignmentCenter;
    [_workLifetime mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(_advance.mas_right);
        make.right.mas_equalTo(_extend.mas_left);
        make.height.equalTo(_lifetimeBack);
    }];
}
// 提前上班响应
- (void)advanceAction:(UIButton *)btn{
    [_advancePick remove];
    [_extendPick remove];
    
    _advancePick = [[DIYPickView alloc] initPickviewWithArray:@[@"6:00", @"7:00"] isHaveNavControler:YES];
    _advancePick.delegate = self;
    
    [_advancePick show];
}
// 延长下班响应
- (void)extendAction:(UIButton *)btn{
    [_advancePick remove];
    [_extendPick remove];

    _extendPick = [[DIYPickView alloc] initPickviewWithArray:@[@"20:00", @"21:00"] isHaveNavControler:NO];
    _extendPick.delegate = self;
    
    [_extendPick show];
}
// pickView代理方法
- (void)toobarDonBtnHaveClick:(DIYPickView *)pickView resultString:(NSString *)resultString{
    
    if (pickView == _advancePick){
        _workLifetime.text = [[resultString stringByAppendingString:@" - " ] stringByAppendingString:_getOffWork];
        _goToWork = resultString;
    }else{
        _workLifetime.text = [[_goToWork stringByAppendingString:@" - " ] stringByAppendingString:resultString];
        _getOffWork = resultString;
    }
}
// 没有订单
- (void)noOrder{
    _prompt = [UILabel new];
    _prompt.backgroundColor = [UIColor whiteColor];
    _prompt.text = @"目前没有订单";
    [self.view addSubview:_prompt];
    [_prompt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(0);
    }];
    
    
}

#pragma mark- tableView相关代理
// tableViewConfig
- (void)tableViewConfig{
    if (!_tableView ) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, KTopHeight + 40, KScreenWidth, KScreenHeight - (KTopHeight + 40 + KTabBarHeight)) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = CELLHIEGTH;
        [self.view addSubview:_tableView];
    }
    
    [_tableView registerClass:[WWYUnfinishedTableViewCell class] forCellReuseIdentifier:@"UnfinishiCell"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //return _listsArray.count;
    return 5;
}
// tableViewDelegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"UnfinishiCell";
    WWYUnfinishedTableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:identifier];
    [cell unfinishedCellCheckBtnBlock:^{
        WWYOrderDetailViewController *detail = [[WWYOrderDetailViewController alloc] init];
        [self.navigationController pushViewController:detail animated:YES];
    }];
    cell.selected = NO;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark 数据本地化 容联账号初始化
- (void)viewWillAppear:(BOOL)animated{
    //动态的说
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
    
    // 本地化 建表(这个地方我觉得最好放在AppDelegate)
    if (NOEmptyStr(KgetUserValueByParaName(USERID))){
        DBDataModel *DBModel = [DBDataModel strandModel];
        
    }
    
    if ([DemoGlobalClass sharedInstance].isLogin == NO)
    {
        
        RlAccountModel *model = [RlAccountModel getRlAcount];
        if (model){
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                NSString *userName = model.voipAccount; //
                NSString *password = model.voipPwd; //
                
                ECLoginInfo *loginInfo = [[ECLoginInfo alloc] init];
                loginInfo.username = userName;
                loginInfo.userPassword = password;
                loginInfo.appKey = [DemoGlobalClass sharedInstance].appKey;
                loginInfo.appToken = [DemoGlobalClass sharedInstance].appToken;
                loginInfo.authType = LoginAuthType_PasswordAuth;
                loginInfo.mode = LoginMode_InputPassword;
                
                [DemoGlobalClass sharedInstance].userPassword = password;
                [[DeviceDBHelper sharedInstance] openDataBasePath:userName];
                [DemoGlobalClass sharedInstance].isHiddenLoginError = NO;
                
                
                [[ECDevice sharedInstance] SwitchServerEvn:NO];
                [[ECDevice sharedInstance] login:loginInfo completion:^(ECError *error){
                    [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_onConnected object:error];
                    if (error.errorCode == ECErrorType_NoError) {
                        [DemoGlobalClass sharedInstance].isLogin = YES;
                        [DemoGlobalClass sharedInstance].userName = userName;
                        //------------
                        /*if([[NSUserDefaults standardUserDefaults] objectForKey:DeviceToken]){
                         NSData *deviceToken= (NSData *)[[NSUserDefaults standardUserDefaults] objectForKey:DeviceToken];
                         [[ECDevice sharedInstance] application:self.appDel didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];//用于聊天推送
                         }*/
                        //----------
                        ECPersonInfo *person = [[ECPersonInfo alloc] init];
                        NSMutableString *userName = [[NSMutableString  alloc] initWithString:[[NSUserDefaults standardUserDefaults] objectForKey:HCuserName]];
                        NSMutableString *userMobile= [[NSMutableString alloc] initWithString:[[NSUserDefaults standardUserDefaults] objectForKey:HCmobile]];
                        if(NOEmptyStr(userName)){
                            if ([userName isEqualToString:userMobile]) {
                                [userName replaceCharactersInRange:NSMakeRange(3, 6) withString:@"****"];
                                person.nickName = userName;
                            }else{
                                person.nickName = userName;
                            }
                            
                        }else if(NOEmptyStr(userMobile)){
                            [userMobile replaceCharactersInRange:NSMakeRange(3, 6) withString:@"****"];
                            person.nickName =userMobile;
                        }
                        
                        if(NOEmptyStr([[NSUserDefaults standardUserDefaults] objectForKey:HCsex])){
                            if([[[NSUserDefaults standardUserDefaults] objectForKey:HCsex] isEqualToString:@"男"]){
                                person.sex = ECSexType_Male;
                            }else if([[[NSUserDefaults standardUserDefaults] objectForKey:HCsex] isEqualToString:@"女"]){
                                person.sex =ECSexType_Female;
                            }
                        }
                        //person.birth = @"2013-11-28";//时间格式 yyyy-MM-dd
                        
                        /**
                         @brief 设置个人信息
                         @param person 个人信息
                         @param completion 执行结果回调block
                         */
                        [[ECDevice sharedInstance] setPersonInfo:person
                                                      completion:^(ECError *error, ECPersonInfo *person) {
                                                          
                                                          if (error.errorCode == ECErrorType_NoError) {
                                                              MyLog(@"修改成功");
                                                          }else{
                                                              MyLog(@"errorCode:%d\rerrorDescription:%@",
                                                                    (int)error.errorCode,error.errorDescription);
                                                          }
                                                      }];
                    }
                }];

       
                
            });
        }
        
    }
    
}


@end
