//
//  ArrayDataSource.h
//  YunboService2016
//
//  Created by 王维一 on 16/3/24.
//  Copyright © 2016年 CarService. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^TableViewCellConfigBlock)(id Cell, id item);

@interface ArrayDataSource : NSObject <UITableViewDataSource>

- (id)initWithItems:(NSArray *)items cellIdentifier:(NSString *)aCellIdentifier configCellBlock:(TableViewCellConfigBlock)aConfigCellBlock;
- (id)itemAtIndexPath:(NSIndexPath *)indexPath;



@end
